#use wml::debian::translation-check translation="0cc922cd661d77cfeed7f482bce1cfba75c197ae"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto varias vulnerabilidades en el núcleo Linux que
pueden dar lugar a elevación de privilegios, a denegación de servicio o a fuga
de información.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12207">CVE-2018-12207</a>

    <p>Se descubrió que, en las CPU Intel que soportan virtualización
    hardware con tablas de páginas extendidas (EPT por sus siglas en inglés), una máquina virtual huésped puede
    manipular el hardware de gestión de memoria para provocar un MCE (Machine Check Error, o «error
    hardware») y denegación de servicio (cuelgue o caída).</p>

    <p>El huésped desencadena este error mediante la modificación de las tablas de páginas sin un
    vaciado («flush») de la TLB, de forma que tanto la entrada 4 KB como la 2 MB de la misma dirección
    virtual se cargan en la TLB de instrucciones (iTLB). Esta actualización
    implementa una mitigación para KVM que impide que las máquinas virtuales huéspedes
    carguen entradas 2 MB en la iTLB. Esto reducirá el rendimiento
    de las máquinas virtuales huéspedes.</p>

    <p>Se puede encontrar más información sobre la mitigación en
    <url "https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/multihit.html">
    o en los paquetes linux-doc-4.9 y linux-doc-4.19.</p>

    <p>A través del DSA 4566-1 se proporcionará una actualización para qemu que añadirá soporte
    para la funcionalidad PSCHANGE_MC_NO, la cual permite inhabilitar las mitigaciones
    iTLB Multihit en hipervisores anidados.</p>

    <p>Puede encontrar la explicación de Intel sobre el problema en
    <url "https://software.intel.com/security-software-guidance/insights/deep-dive-machine-check-error-avoidance-page-size-change-0">.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0154">CVE-2019-0154</a>

    <p>Intel descubrió que, en sus GPU de octava y novena generaciones,
    la lectura de ciertos registros mientras la GPU está en un estado de bajo consumo de energía
    puede provocar que el sistema se cuelgue. Un usuario local autorizado a utilizar la GPU
    puede usar esto para provocar denegación de servicio.</p>

    <p>Esta actualización mitiga el problema mediante cambios en el controlador
    i915.</p>

    <p>Los chips afectados (gen8 y gen9) están listados en
    <url "https://en.wikipedia.org/wiki/List_of_Intel_graphics_processing_units#Gen8">.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0155">CVE-2019-0155</a>

    <p>Intel descubrió que a sus GPU de novena generación y posteriores les
    falta una comprobación de seguridad en el Blitter Command Streamer (BCS). Un
    usuario local autorizado a utilizar la GPU podría usar esto para acceder a cualquier
    memoria a la que tenga acceso la GPU, lo que podría dar lugar a denegación
    de servicio (corrupción de memoria o caída), a fuga de información
    sensible o a elevación de privilegios.</p>

    <p>Esta actualización mitiga el problema añadiendo la comprobación de seguridad al
    controlador i915.</p>

    <p>Los chips afectados (gen9 y siguientes) están listados en
    <url "https://en.wikipedia.org/wiki/List_of_Intel_graphics_processing_units#Gen9">.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11135">CVE-2019-11135</a>

    <p>Se descubrió que en las CPU Intel que soportan memoria
    transaccional (TSX), una transacción que va a ser abortada puede
    continuar ejecutándose de forma especulativa, leyendo datos sensibles de
    regiones internas de la memoria y filtrándolos por medio de operaciones dependientes.
    Intel llama a esto <q>TSX Asynchronous Abort</q> (TAA).</p>

    <p>Para las CPU afectadas por los problemas de muestreo de datos de microarquitectura (MDS
    por sus siglas en inglés):
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-12126">CVE-2018-12126</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-12127">CVE-2018-12127</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-12130">CVE-2018-12130</a> y
    <a href="https://security-tracker.debian.org/tracker/CVE-2019-11091">CVE-2019-11091</a>,
    la mitigación existente también mitiga este problema.</p>

    <p>Para procesadores vulnerables a TAA pero no a MDS, esta actualización
    inhabilita TSX por omisión. Esta mitigación requiere microcódigo de CPU
    actualizado. Se proporcionará un paquete intel-microcode actualizado (disponible
    solo en Debian non-free) mediante el DSA 4565-1. El microcódigo de
    CPU actualizado puede estar disponible también como parte de una actualización del
    firmware del sistema ("BIOS").</p>

    <p>Se puede encontrar más información sobre la mitigación en
    <url "https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/tsx_async_abort.html">
    o en los paquetes linux-doc-4.9 y linux-doc-4.19.</p>

    <p>Puede encontrar la explicación de Intel sobre el problema en
    <url "https://software.intel.com/security-software-guidance/insights/deep-dive-intel-transactional-synchronization-extensions-intel-tsx-asynchronous-abort">.</p></li>

</ul>

<p>Para la distribución «antigua estable» (stretch), estos problemas se han corregido
en la versión 4.9.189-3+deb9u2.</p>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 4.19.67-2+deb10u2.</p>

<p>Le recomendamos que actualice los paquetes de linux.</p>

<p>Para información detallada sobre el estado de seguridad de linux, consulte su página
en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4564.data"
