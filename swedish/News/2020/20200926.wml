<define-tag pagetitle>Uppdaterad Debian 10; 10.6 utgiven</define-tag>
<define-tag release_date>2020-09-26</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="cc3aa11466129a6224ab33a305a554cb8d65f63c"

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.6</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Debianprojektet presenterar stolt sin sjätte uppdatering till dess
stabila utgåva Debian <release> (med kodnamnet <q><codename></q>). 
Denna punktutgåva lägger huvudsakligen till rättningar för säkerhetsproblem,
tillsammans med ytterligare rättningar för allvarliga problem. Säkerhetsbulletiner
har redan publicerats separat och refereras när de finns tillgängliga.</p>

<p>Vänligen notera att punktutgåvan inte innebär en ny version av Debian 
<release> utan endast uppdaterar några av de inkluderade paketen. Det behövs
inte kastas bort gamla media av <q><codename></q>. Efter installationen
kan paket uppgraderas till de aktuella versionerna genom att använda en uppdaterad
Debianspegling..</p>

<p>De som frekvent installerar uppdateringar från security.debian.org kommer inte att behöva
uppdatera många paket, och de flesta av sådana uppdateringar finns
inkluderade i punktutgåvan.</p>

<p>Nya installationsavbildningar kommer snart att finnas tillgängliga på de vanliga platserna.</p>

<p>En uppgradering av en existerande installation till denna revision kan utföras genom att
peka pakethanteringssystemet på en av Debians många HTTP-speglingar.
En utförlig lista på speglingar finns på:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Blandade felrättningar</h2>

<p>Denna uppdatering av den stabila utgåvan lägger till några viktiga felrättningar till följande packages.</p>

<p>Notera att, på grund av byggproblem finns inte uppdateringarna för paketen cargo, rustc och rustc-bindgen tillgängliga för
arkitekturen <q>armel</q>. De kan komma att läggas till vid ett senare tillfälle om problemen
löses.</p>

<table border=0>
<tr><th>Paket</th>               <th>Orsak</th></tr>
<correction arch-test "Fix detection of s390x sometimes failing">
<correction asterisk "Fix crash when negotiating for T.38 with a declined stream [CVE-2019-15297], <q>SIP request can change address of a SIP peer</q> [CVE-2019-18790], <q>AMI user could execute system commands</q> [CVE-2019-18610], segfault in pjsip show history with IPv6 peers">
<correction bacula "Fix <q>oversized digest strings allow a malicious client to cause a heap overflow in the director's memory</q> [CVE-2020-11061]">
<correction base-files "Update /etc/debian_version for the point release">
<correction calamares-settings-debian "Disable displaymanager module">
<correction cargo "New upstream release, to support upcoming Firefox ESR versions">
<correction chocolate-doom "Fix missing validation [CVE-2020-14983]">
<correction chrony "Prevent symlink race when writing to the PID file [CVE-2020-14367]; fix temperature reading">
<correction debian-installer "Update Linux ABI to 4.19.0-11">
<correction debian-installer-netboot-images "Rebuild against proposed-updates">
<correction diaspora-installer "Use --frozen option to bundle install to use upstream Gemfile.lock; don't exclude Gemfile.lock during upgrades; don't overwrite config/oidc_key.pem during upgrades; make config/schedule.yml writeable">
<correction dojo "Fix prototype pollution in deepCopy method [CVE-2020-5258] and in jqMix method [CVE-2020-5259]">
<correction dovecot "Fix dsync sieve filter sync regression; fix handling of getpwent result in userdb-passwd">
<correction facter "Change Google GCE Metadata endpoint from <q>v1beta1</q> to <q>v1</q>">
<correction gnome-maps "Fix an issue with misaligned shape layer rendering">
<correction gnome-shell "LoginDialog: Reset auth prompt on VT switch before fade in [CVE-2020-17489]">
<correction gnome-weather "Prevent a crash when the configured set of locations are invalid">
<correction grunt "Use safeLoad when loading YAML files [CVE-2020-7729]">
<correction gssdp "New upstream stable release">
<correction gupnp "New upstream stable release; prevent the <q>CallStranger</q> attack [CVE-2020-12695]; require GSSDP 1.0.5">
<correction haproxy "logrotate.conf: use rsyslog helper instead of SysV init script; reject messages where <q>chunked</q> is missing from Transfer-Encoding [CVE-2019-18277]">
<correction icinga2 "Fix symlink attack [CVE-2020-14004]">
<correction incron "Fix cleanup of zombie processes">
<correction inetutils "Fix remote code execution issue [CVE-2020-10188]">
<correction libcommons-compress-java "Fix denial of service issue [CVE-2019-12402]">
<correction libdbi-perl "Fix memory corruption in XS functions when Perl stack is reallocated [CVE-2020-14392]; fix a buffer overflow on an overlong DBD class name [CVE-2020-14393]; fix a NULL profile dereference in dbi_profile() [CVE-2019-20919]">
<correction libvncserver "libvncclient: bail out if UNIX socket name would overflow [CVE-2019-20839]; fix pointer aliasing/alignment issue [CVE-2020-14399]; limit max textchat size [CVE-2020-14405]; libvncserver: add missing NULL pointer checks [CVE-2020-14397]; fix pointer aliasing/alignment issue [CVE-2020-14400]; scale: cast to 64 bit before shifting [CVE-2020-14401]; prevent OOB accesses [CVE-2020-14402 CVE-2020-14403 CVE-2020-14404]">
<correction libx11 "Fix integer overflows [CVE-2020-14344 CVE-2020-14363]">
<correction lighttpd "Backport several usability and security fixes">
<correction linux "New upstream stable release; increase ABI to 11">
<correction linux-latest "Update for -11 Linux kernel ABI">
<correction linux-signed-amd64 "New upstream stable release">
<correction linux-signed-arm64 "New upstream stable release">
<correction linux-signed-i386 "New upstream stable release">
<correction llvm-toolchain-7 "New upstream release, to support upcoming Firefox ESR versions; fix bugs affecting rustc build">
<correction lucene-solr "Fix security issue in DataImportHandler configuration handling [CVE-2019-0193]">
<correction milkytracker "Fix heap overflow [CVE-2019-14464], stack overflow [CVE-2019-14496], heap overflow [CVE-2019-14497], use after free [CVE-2020-15569]">
<correction node-bl "Fix over-read vulnerability [CVE-2020-8244]">
<correction node-elliptic "Prevent malleability and overflows [CVE-2020-13822]">
<correction node-mysql "Add localInfile option to control LOAD DATA LOCAL INFILE [CVE-2019-14939]">
<correction node-url-parse "Fix insufficient validation and sanitization of user input [CVE-2020-8124]">
<correction npm "Don't show password in logs [CVE-2020-15095]">
<correction orocos-kdl "Remove explicit inclusion of default include path, fixing issues with cmake &lt; 3.16">
<correction postgresql-11 "New upstream stable release; set a secure search_path in logical replication walsenders and apply workers [CVE-2020-14349]; make contrib modules' installation scripts more secure [CVE-2020-14350]">
<correction postgresql-common "Don't drop plpgsql before testing extensions">
<correction pyzmq "Asyncio: wait for POLLOUT on sender in can_connect">
<correction qt4-x11 "Fix buffer overflow in XBM parser [CVE-2020-17507]">
<correction qtbase-opensource-src "Fix buffer overflow in XBM parser [CVE-2020-17507]; fix clipboard breaking when timer wraps after 50 days">
<correction ros-actionlib "Load YAML safely [CVE-2020-10289]">
<correction rustc "New upstream release, to support upcoming Firefox ESR versions">
<correction rust-cbindgen "New upstream release, to support upcoming Firefox ESR versions">
<correction ruby-ronn "Fix handling of UTF-8 content in manpages">
<correction s390-tools "Hardcode perl dependency instead of using ${perl:Depends}, fixing installation under debootstrap">
</table>


<h2>Säkerhetsuppdateringar</h2>


<p>Denna revision lägger till följande säkerhetsuppdateringar till den stabila utgåvan.
Säkerhetsgruppen har redan släppt bulletiner för alla dessa
uppdateringar:</p>

<table border=0>
<tr><th>Bulletin-ID</th>  <th>Paket</th></tr>
<dsa 2020 4662 openjdk-11>
<dsa 2020 4734 openjdk-11>
<dsa 2020 4736 firefox-esr>
<dsa 2020 4737 xrdp>
<dsa 2020 4738 ark>
<dsa 2020 4739 webkit2gtk>
<dsa 2020 4740 thunderbird>
<dsa 2020 4741 json-c>
<dsa 2020 4742 firejail>
<dsa 2020 4743 ruby-kramdown>
<dsa 2020 4744 roundcube>
<dsa 2020 4745 dovecot>
<dsa 2020 4746 net-snmp>
<dsa 2020 4747 icingaweb2>
<dsa 2020 4748 ghostscript>
<dsa 2020 4749 firefox-esr>
<dsa 2020 4750 nginx>
<dsa 2020 4751 squid>
<dsa 2020 4752 bind9>
<dsa 2020 4753 mupdf>
<dsa 2020 4754 thunderbird>
<dsa 2020 4755 openexr>
<dsa 2020 4756 lilypond>
<dsa 2020 4757 apache2>
<dsa 2020 4758 xorg-server>
<dsa 2020 4759 ark>
<dsa 2020 4760 qemu>
<dsa 2020 4761 zeromq3>
<dsa 2020 4762 lemonldap-ng>
<dsa 2020 4763 teeworlds>
<dsa 2020 4764 inspircd>
<dsa 2020 4765 modsecurity>
</table>



<h2>Debianinstalleraren</h2>
<p>Installeraren har uppdaterats för att inkludera rättningarna som har inkluderats i den
stabila utgåvan med denna punktutgåva.</p>

<h2>URLer</h2>

<p>Den fullständiga listan på paket som har förändrats i denna revision:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Den aktuella stabila utgåvan:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Föreslagna uppdateringar till den stabila utgåvan:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Information om den stabila utgåvan (versionsfakta, kända problem osv.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Säkerhetsbulletiner och information:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Om Debian</h2>

<p>Debianprojektet är en grupp utvecklare av Fri mjukvara som
donerar sin tid och kraft för att producera det helt
fria operativsystemet Debian.</p>

<h2>Kontaktinformation</h2>

<p>För ytterligare information, vänligen besök Debians webbplats på
<a href="$(HOME)/">https://www.debian.org/</a>, skicka e-post till
&lt;press@debian.org&gt;, eller kontakta gruppen för stabila utgåvor på
&lt;debian-release@lists.debian.org&gt;.</p>
