#use wml::debian::template title="Debian GNU/Hurd --- Utveckling" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/hurd/menu.inc"
#use wml::debian::translation-check translation="e42a3c19fa8c376678e6147f47b31ba3fc60e369"

<h1>
Debian GNU/Hurd</h1>
<h2>
Utveckling av Distributionen</h2>

<h3>
Paketering av Hurd-mjukvara</h3>
<p>
Hurd-specifika paket underhålls på <url "https://salsa.debian.org/hurd-team/">.
</p>

<h3>Anpassa Debianpaket</h3>
<p>
	Om du vill hjälpa Debian GNU/Hurd-anpassningen, så bör du göra dig
	bekant med Debians paketeringssystem. När du har gjort detta genom att
	läsa dokumentationen och besöka <a href="$(HOME)/devel/">utvecklarhörnan</a>
	så bör du veta hur man extraherar ett Debian-källkodspaket och bygger ett
	Debianpaket. Här följer en snabbkurs för latmaskar:
</p>

<h3>Få tag på källkod och bygga paket</h3>
<p>
	Att skaffa källkoden kan enkelt göras genom att köra <code>apt source
	paket</code>, vilket även extraherar källkoden.
</p>
<p>
	Att packa upp ett Debian-källkodspaket kräver filen
	<code>paket_version.dsc</code> och filerna som listas i denna. Du skapar
	Debians byggmapp med kommandot
	<code>dpkg-source -x paket_version.dsc</code>
</p>

<p>
	Bygge av ett paket sker i den nu skapade Debianbyggmappen
	<code>package-version</code> med hjälp av kommandot
	<code>dpkg-buildpacakge -B "-mMittnamn &lt;MinEmail&gt;"</code>.
	Istället för <code>-B</code> kan du använda
	<code>-b</code> om du även vill bygga de arkitekturoberoende delarna
	av paketet (Men det är oftast meningslöst eftersom de redan finns
	tillgängliga i arkivet, och att bygga dem kan kräva ytterligare beroenden).
	Du kan lägga till <code>-uc</code> för att undvika att
	signera paketet med din pgp-nyckel.
</p>

<p>
	Att bygga paketet kan kräva ytterligare installerade paket. Enklaste sättet
	att installera dessa är <code>apt build-dep paket</code> vilket installerar
	alla paket som krävs.
</p>

<p>
    Att använda pbuilder kan vara bekvämt. Det kan byggas med
    <code>sudo pbuilder create --mirror http://deb.debian.org/debian-ports/ --debootstrapopts --keyring=/usr/share/keyrings/debian-ports-archive-keyring.gpg --debootstrapopts --extra-suites=unreleased --extrapackages debian-ports-archive-keyring</code>
    och sedan kan man använda <code>pdebuild -- --binary-arch</code> vilket kommer
    att hantera hämtning av byggberoenden, osv., och placera resultatet i
    <code>/var/cache/pbuilder/result</code>
</p>

<h3>Välj ett paket</h3>
<p>
	Vilket paket skall det arbetas på? Varje paket som inte ännu är anpassat
	beöver det jobbas på. Detta ändrar sig hela tiden, så det uppskattas om man
	först koncentrerar sig på paket med många bakåtberoenden, vilket kan ses
	i paketberoendegrafen
	<url "https://people.debian.org/~sthibault/graph-radial.pdf"> som uppdateras
	dagligen, eller på listan över mest efterfrågade paket
	<url "https://people.debian.org/~sthibault/graph-total-top.txt"> (detta är
	långtidslistan, korttidslistan är
	<url "https://people.debian.org/~sthibault/graph-top.txt">). Det är också
	en god idé att välja paket från listorna på föråldrade paket,
	<url "https://people.debian.org/~sthibault/out_of_date2.txt"> och
	<url "https://people.debian.org/~sthibault/out_of_date.txt">, eftersom dessa
	brukade fungera, men nu inte gör det av några mindre orsaker.
	Du kan också bara välja ett av de saknade paketen slumpmässigt, eller
	kontrollera autobyggloggar på sändlistan debian-hurd-build-logs, eller
	använda wanna-build-listan från
	<url "https://people.debian.org/~sthibault/failed_packages.txt">.
</p>

<p>
Kolla även om arbete har gjorts på
<url "https://alioth.debian.org/tracker/?atid=410472&amp;group_id=30628&amp;func=browse">,
<url "https://alioth.debian.org/tracker/?atid=411594&amp;group_id=30628&amp;func=browse">,
och i BTS (<url "https://bugs.debian.org/cgi-bin/pkgreport.cgi?users=debian-hurd@lists.debian.org;tag=hurd">), och <url "https://wiki.debian.org/Debian_GNU/Hurd">,
samt aktuell status av paket på buildd.debian.org, t.ex.
<url "https://buildd.debian.org/util-linux">.
</p>

<h4>
Paket som inte anpassas</h4>
<p>
	Några av dessa paket, eller delar av dem, kan bli anpassningsbara senare, men
	för närvarande är dem åtminstone ansedda att inte vara anpassningsbara.
	Normalt markeras dessa med NotForUs i buildd-databasen.
	
</p>

<ul>
<li>
<code>base/makedev</code>, eftersom Hurd kommer med sin egna version av detta
skript. Debians källkodspaket innehåller endast en Linux-specifik version.</li>
<li>
<code>base/modconf</code> och <code>base/modutils</code>, eftersom moduler
är ett koncept som är specifikt för Linux.</li>
<li>
<code>base/netbase</code>, eftersom kvarvarande saker som finns där är högst
specifikt för Linuxkärnan. Hurd använder
<code>inetutils</code> istället.</li>
<li>
<code>base/pcmcia-cs</code>, eftersom detta paket är Linuxspecifikt).</li>
<li>
<code>base/setserial</code>, eftersom det är specifikt för Linuxkärnan.
Dock så kan vi möjligen använda det med hjälp av anpassningen av
Linux char-drivrutiner till GNU Mach.</li>
</ul>

<h3> <a name="porting_issues">
Allmänna anpassningsproblem</a></h3>
<p>
<a href=https://www.gnu.org/software/hurd/hurd/porting/guidelines.html>En lista på
vanliga problem</a> finns tillgänglig på uppströmswebbsidan. Följande vanliga
problem är specifika för Debian.</p>
<p>Innan du försöker rätta något, komtrollera om kanske kfreebsd*-anpassningen
redan har en fix, som kanske endast måste utökas till hurd-i386.</p>

<ul>
<li>
<code>foo : Depends: foo-data (= 1.2.3-1) but it is not going to be installed</code>
<p>
Det korta svaret är: paketet <code>foo</code> misslyckades att bygga på hurd-i386,
och det måste fixas, kolla på byggfelet på dess statussida på buildd.debian.org.
</p>
<p>
Detta händer vanligtvis när paketet <code>foo</code> för närvarande misslyckas
att bygga, men byggde utan problem tidigare. Använd <code>apt-cache policy foo foo-data</code>
för att se att exempelvis version <code>1.2.3-1</code> av <code>foo</code>
finns tillgänglig, och en nyare <code>foo-data</code> version <code>2.0-1</code>
finns tillgänglig. Detta är på grund av att på debian-ports, delas
arkitekturoberoende paket mellan alla arkitekturer, och därför när en ny
version av källkodspaket <code>foo</code> (som bygger de binära paketen
<code>foo</code> och <code>foo-data</code>) laddas upp kommer det nyare
arch:all paketet <code>foo-data</code> installeras, även om den nyare
hurd-i386 binära paketet <code>foo</code> inte kan byggas, vilket leder till
inkompatibla versioner. Att rätta detta kräver att debian-ports-arkivet använder
dak istället för mini-dak, vilket fortfarande är ett pågående arbete.
</p>

</li>
<li>
<code>some symbols or patterns disappeared in the symbols file</code>
<p>
Some packages maintain a list of the symbols that are expected to appear in
libraries. This list is however usually obtained on a Linux system, and thus
include symbols which may not make sense on non-Linux systems (e.g. due a
Linux-only feature). One can however introduce conditionals in the
<code>.symbols</code> file, for instance:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
 (arch=linux-any)linuxish_function@Base 1.23
</pre></td></tr></table>

</li>
<li>

<code>Felaktiga libc6-beroenden</code>
<p>
Några paket använder ett felaktigt beroende på <code>libc6-dev</code>. Detta
är felaktigt eftersom <code>libc6</code> är specifikt för vissa arkitekturer
av GNU/Linux. Motsvarande paket för GNU är <code>libc0.3-dev</code> men andra
OS kommer ha andra beroenden. Du kan lokalisera problemet i
<code>debian/control</code>-filen i källkodsträdet. En typisk lösning inkluderar
att detektera OSet med hjälp av <code>dpkg-architecture</code> samt
hårdkoda so-namnet, eller (vilket är bättre), använda en logisk OR, exempelvis:
<code>libc6-dev | libc6.1-dev | libc0.3-dev | libc0.1-dev | libc-dev</code>.
<code>libc-dev</code> är ett virtuellt paket som fungerar för alla so-namn, men
du kan endast använda det som det sista alternativet.
</p></li>
<li>
<code>odefinierad referens till snd_*, SND_* odefinierad</code>
<p>
Några paket använder ALSA även på icke-Linux-arkitekturer. Paketet oss-libalsa
ger emulering via OSS, men den är begränsad till 1.0.5, och några funktioner
ges inte, så som sequencer-hantering.</p>
<p>
Om paketet tillåter det, skall alsa-stöd avaktiveras på arkitekturerna
<code>!linux-any</code> (exempelvis genom en <code>configure</code>-inställning),
och en <code>[linux-any]</code>-qualifier tillagd till
alsas <code>Build-Depends</code>, och motsatsen läggs till
<code>Build-Conflicts</code>, så som
<code>Build-Conflicts: libasound2-dev [!linux-any]</code>.
</p>
</li>
<li>
<code>dh_install: Cannot find (any matches for) "foo" (tried in ., debian/tmp)</code>
<p>
Detta händer vanligtvis när uppström inte installerade något eftersom det inte
kände igen operativsystemet. Ibland är det bara dumt (t.ex. när den inte vet
att det är precis likadant att bygga ett delat bibliotek på GNU/Hurd som på
GNU/Linux) och det behöver rättas. Ibland är det faktiskt korrekt (t.ex. att
inte installera systemd-servicefiler). I detta fall kan man använda sig av
dh-exec: lägg till en build-depend på <tt>dh-exec</tt>, kör <tt>chmod +x</tt>
på <tt>.install</tt>-filen, och lägg till exempelvis <tt>[linux-any]</tt> eller
<tt>[!hurd-any]</tt> till de problematiska raderna.
</p>
</li>
</ul>


<h3> <a name="debian_installer">
Hacka med Debian-installeraren</a></h3>

<p>
För att bygga en ISO-avbildning är det enklaste att börja med en existerande
från <a href=hurd-cd>sidan för Hurd CD-avbildningar</a>. Du kan sedan montera den och kopiera den:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
mount debian-sid-hurd-i386-NETINST-1.iso /mnt
cp -a /mnt /tmp/myimage
umount /mnt
chmod -R +w /tmp/myimage
</pre></td></tr></table>

<p>
Du kan montera den initiala ramdisken och exempelvis ersätta en översättare med din egen version:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
gunzip /tmp/myimage/initrd.gz
mount /tmp/myimage/initrd /mnt
cp ~/hurd/rumpdisk/rumpdisk /mnt/hurd/
umount /mnt
gzip /tmp/myimage/initrd
</pre></td></tr></table>

<p>
Nu kan du bygga om ison med grub-mkrescue:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
rm -fr /tmp/myimage/boot/grub/i386-pc
grub-mkrescue -o /tmp/myimage.iso /tmp/myimage
</pre></td></tr></table>

