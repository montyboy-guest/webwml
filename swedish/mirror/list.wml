#use wml::debian::template title="Debians världsomspännande spegelplatser" MAINPAGE="true"
#use wml::debian::translation-check translation="b554c22abdbc4a6253951c9bf9610405d0c4f2cd"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#per-country">Debianspeglar per Land</a></li>
  <li><a href="#complete-list">Fullständig lista på speglar</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian distribueras
(<a href="https://www.debian.org/mirror/">speglas</a>) på hundratals servrar. 
Om du planerar att <a href="../download">hämta</a> Debian, välj en server
som är nära dig först. Detta kommer förmodligen att vara snabbare och reducerar
även lasten på våra centrala servrar.</p>
</aside>


<p class="centerblock">
   Debianspeglingar finns i många länder, och för några av dessa har vi lagt
   till ett <code>ftp.&lt;country&gt;.debian.org</code>-alias. Detta
   alias pekar vanligtvis till en spegling som synkroniserar regelbundet
   och snabbt och som innehåller alla Debians arkitekturer. Debianarkivet
   finns alltid tillgängligt via HTTP på platsen
   <code>/debian</code> på servern.
</p>

<p class="centerblock">
   Andra speglingssajter kan ha restriktioner på vad
   de speglar (på grund av utrymmesrestriktioner). Bara på grund av att
   en sajt inte är landets <code>ftp.&lt;country&gt;.debian.org</code> betyder
   inte nödvändigtvis att den är långsammare eller mindre uppdaterad än
   <code>ftp.&lt;country&gt;.debian.org</code>-speglingen. Faktum är att
   en spegling som innehåller din arkitektur och är närmare dig som användare,
   och därmed snabbare, föredras nästan alltid före andra speglingar som
   är längre bort.
</p>

<p>Använd platsen närmast dig för snabbast möjliga nedladdning -
oavsett om det är landets speglingsalias eller inte.
Programmet
<a href="https://packages.debian.org/stable/net/netselect">\
<code>netselect</code></a> kan användas för att
avgöra vilken plats som har minst latens; använd ett hämtningsprogram såsom
<a href="https://packages.debian.org/stable/web/wget">\
<code>wget</code></a> eller
<a href="https://packages.debian.org/stable/net/rsync">\
<code>rsync</code></a> för att avgöra vilken plats som ger högst nedladdningshastigheter.
Notera att geografisk närhet ofta inte är den viktigaste faktorn för att avgöra
vilken maskin som kommer ge dig högst hastigheter och pålitlighet.</p>

<p>
Om ditt system flyttas mycket så kan du vara bäst betjänt av en "spegling"
som drivs av en global <abbr title="Content Delivery Network - Innehållsleveransnätverk">CDN</abbr>.
Debianprojektet underhåller <code>deb.debian.org</code> för detta
ändamål och du kan använda det i din <code>sources.list</code>; Se
<a href="http://deb.debian.org">tjänstens webbplats</a> för ytterligare detaljer.</p>

<h2 id="per-country">Debians speglingsalias per land</h2>

<table border="0" class="center">
<tr>
  <th>Land</th>
  <th>Plats</th>
  <th>Arkitekturer</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-primary.inc"
</table>

<h2 id="complete-list">Fullständig lista på speglingar av Debianarkivet</h2>

<table border="0" class="center">
<tr>
  <th>Värdnamn</th>
  <th>HTTP</th>
  <th>Arkitekturer</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-secondary.inc"
</table>

#include "$(ENGLISHDIR)/mirror/list-footer.inc"
