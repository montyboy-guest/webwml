#use wml::debian::template title="LTS informações de segurança" GEN_TIME="yes"
#use wml::debian::toc
#use wml::debian::recent_list_security
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="51b1f2756d728aa0beb3ba7e5f67a15ab3b8db98"

<define-tag toc-title-formatting endtag="required" whitespace="delete">
<h2>%body</h2>
</define-tag>

<toc-display/>

<toc-add-entry name="keeping-secure">Mantendo o seu sistema Debian LTS system seguro</toc-add-entry>

<p>Para receber os últimos alertas de segurança do Debian LTS, inscreva-se
na lista de discussão <a href="https://lists.debian.org/debian-lts-announce/">\
debian-lts-announce</a>.</p>

<p>Para mais informações sobre questões de segurança no Debian,
por favor acesse a página
the <a href="../../security">informações sobre segurança</a>.</p>

<a class="rss_logo" href="dla">RSS</a>
<toc-add-entry name="DLAS">Alertas recentes</toc-add-entry>

<p>Essas páginas contém um imenso repositório de alertas de segurança enviados
para a lista de discussão
<a href="https://lists.debian.org/debian-lts-announce/">\
debian-lts-announce</a>.

<p>
<:= get_recent_security_list( '1m', '6', '.', '$(ENGLISHDIR)/lts/security' ) :>
</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Debian LTS Security Advisories (titles only)" href="dla">
<link rel="alternate" type="application/rss+xml"
 title="Debian LTS Security Advisories (summaries)" href="dla-long">
:#rss#}
<p>Os últimos alertas de segurança do Debian LTS também estão disponíveis em
<a href="dla">formato RDF</a>. Também ofertamos um
<a href="dla-long">segundo arquivo</a> que inclui o primeiro parágrafo
do alerta correspondente para que você possa ver sobre o que ele trata.
</p>

#include "$(ENGLISHDIR)/lts/security/index.include"
<p>Alertas de segurança mais antigos também estão disponíveis:
<:= get_past_sec_list(); :>

<p>Distribuições Debian não são vulneráveis a todos os problemas de segurança.
O <a href="https://security-tracker.debian.org/">Rastreador de Segurança
Debian</a> coleta todas as informações sobre o estado de vulnerabilidade dos
pacotes Debian e pode ser pesquisado por nome CVE ou por pacote.</p>

<toc-add-entry name="contact">>Informações para contato</toc-add-entry>

<p>Por favor leia o <a href="https://wiki.debian.org/LTS/FAQ">FAQ do Debian LTS</a>
antes de nos contatar, pois sua pergunta pode já ter sido respondida nele.</p>

<p>As <a href="https://wiki.debian.org/LTS/FAQ">informações de contato também
estão no FAQ</a>.</p>
