#use wml::debian::template title="Obtendo o Debian"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="fed541893c7d33166fc447f1489de98ee963d562"

<p>O Debian é distribuído <a href="../intro/free">livremente</a>
pela Internet. Você pode baixá-lo integralmente partir de qualquer um dos nossos
<a href="ftplist">espelhos</a> (<q>mirrors</q>).
O <a href="../releases/stable/installmanual">Manual de Instalação</a>
contém instruções detalhadas da instalação e as notas de lançamento podem ser
encontradas <a href="../releases/stable/releasenotes">aqui</a>.</p>

<p>Esta página possui opções para instalar o Debian estável (stable). Se você
estiver interessado(a) no teste (testing) ou instável (unstable), visite nossa
<a href="../releases/">página de versões/lançamentos</a>.</p> 

<div class="line">
  <div class="item col50">
    <h2><a href="netinst">Baixe uma imagem de instalação</a></h2>
    <p>Dependendo da sua conexão de Internet, você pode baixar qualquer uma
    das seguintes opções:</p>
    <ul>
      <li>Uma <a href="netinst"><strong>pequena imagem de instalação</strong></a>:
	    pode ser baixada rapidamente e deve ser gravada em um disco removível.
	    Para usar isso, você precisará de uma máquina com uma conexão de Internet.
	<ul class="quicklist downlist">
          <li><a title="Baixe o instalador para PC Intel e AMD de 64 bits"
                 href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">iso netinstall para PC de 64 bits</a></li>
          <li><a title="Baixe o instalador para PC normal Intel e AMD de 32 bits"
                 href="<stable-images-url/>/i386/iso-cd/debian-<current-tiny-cd-release-filename/>-i386-netinst.iso">iso netinstall para PC de 32 bits</a></li>
       </ul>
      </li>
      <li>Uma imagem <a href="../CD/"><strong>maior de instalação
	completa</strong></a>: contém mais pacotes, tornando mais fácil de instalar
	em máquinas sem uma conexão de Internet.
	<ul class="quicklist downlist">
	  <li><a title="Baixe torrents de DVD para PC Intel e AMD de 64 bits"
	         href="<stable-images-url/>/amd64/bt-dvd/">torrents para PC de 64 bits (DVD)</a></li>
	  <li><a title="Baixe torrents de DVD para PC normal Intel e AMD de 32 bits"
		 href="<stable-images-url/>/i386/bt-dvd/">torrents para PC de 32 bits (DVD)</a></li>
	  <li><a title="Baixe torrents de CD para PC Intel e AMD de 32 bits"
	         href="<stable-images-url/>/amd64/bt-cd/">torrents para PC de 64 bits (CD)</a></li>
	  <li><a title="Baixe torrents de CD para PC normal Intel e AMD de 32 bits"
		 href="<stable-images-url/>/i386/bt-cd/">torrents para PC de 32 bits (CD)</a></li>
        </ul>
      </li>
    </ul>
  </div>
  <div class="item col50 lastcol">
    <h2><a href="https://cloud.debian.org/images/cloud/">Use uma imagem cloud do Debian</a></h2>
    <p>Uma <a href="https://cloud.debian.org/images/cloud/"><strong>imagem cloud</strong></a> oficial,
    construída pela equipe de cloud, pode ser usada no: </p> 
    <ul>
        <li>Seu provedor OpenStack, no formato qcow2 ou raw. 
          <ul class="quicklist downlist">        
            <li>AMD/Intel de 64 bits (<a title="imagem OpenStack para AMD/Intel de 64 bits qcow2 " href="https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-generic-amd64.qcow2">qcow2</a>, <a title="Imagem OpenStack para AMD/Intel de 64 bits raw" href="https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-generic-amd64.raw">raw</a>)</li>
            <li>ARM de 64 bits (<a title="imagem OpenStack para ARM de 64 bits qcow2"  href="https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-generic-arm64.qcow2">qcow2</a>, <a title="imagem OpenStack para ARM de 64 bits raw"  href="https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-generic-arm64.raw">raw</a>)</li>
            <li> Little Endian PowerPC de 64 bits (<a title="imagem OpenStack para Little Endian PowerPC de 64 bits qcow2" href="https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-generic-ppc64el.qcow2">qcow2</a>, <a title="imagem OpenStack para Little Endian PowerPC de 64 raw bits" href="https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-generic-ppc64el.raw">raw</a>)</li>
          </ul>
        </li>
        <li>uma máquina local virtual QEMU, em formato qcow2 ou raw.
          <ul class="quicklist downlist">
               <li>AMD/Intel de 64 bits (<a title="imagem QEMU para AMD/Intel qcow2 de 64 bits" href="https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-nocloud-amd64.qcow2">qcow2</a>, <a title="imagem QEMU para AMD/Intel raw de 64 bits" href="https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-nocloud-amd64.raw">raw</a>)</li>
               <li>ARM de 64 bits (<a title="imagem QEMU para ARM 64 bits qcow2" href="https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-nocloud-arm64.qcow2">qcow2</a>, <a title="imagem QEMU para ARM 64 bits raw" href="https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-nocloud-arm64.raw">raw</a>)</li>
               <li>Little Endian PowerPC de 64 bits (<a title="Imagem QEMU para Little Endian PowerPC de 64 bits qcow2" href="https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-nocloud-ppc64el.qcow2">qcow2</a>, <a title="Imagem QEMU para Little Endian PowerPC de 64 bits raw" href="https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-nocloud-ppc64el.raw">raw</a>)</li>
          </ul>
        </li>
      <li>Amazon EC2, como uma imagem de máquina ou no marketplace da AWS.
          <ul class="quicklist downlist">
            <li><a title="Imagens máquina Amazon" href="https://wiki.debian.org/Cloud/AmazonEC2Image/">Imagens de máquina Amazon</a></li>
            <li><a title="AWS Marketplace" href="https://aws.amazon.com/marketplace/seller-profile?id=4d4d4e5f-c474-49f2-8b18-94de9d43e2c0">Marketplace da AWS</a></li>
          </ul>
      </li>
      <li>Microsoft Azure, no marketplace da Azure.
          <ul class="quicklist downlist">
            <li><a title="Debian 12 no Marketplace da Azure" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-12?tab=PlansAndPrice">Debian 12 ("Bookworm")</a></li>
            <li><a title="Debian 11 no Marketplace da Azure" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-11?tab=PlansAndPrice">Debian 11 ("Bullseye")</a></li>
          </ul>
      </li>
    </ul>
 </div>
</div>
 <div class="line">
  <div class="item col50">
   <h2><a href="../CD/vendors/">Compre um conjunto de CDs ou DVDs de um dos
     vendedores de CDs Debian</a></h2>

   <p>
      Muitos dos vendedores vendem a distribuição por menos que US$5 mais
      o envio (confira as páginas web deles para ver se eles enviam
      internacionalmente).
      <br />
      Alguns dos <a href="../doc/books">livros sobre Debian</a> também vêm com
      CDs.
   </p>

   <p>Aqui estão as vantagens básicas dos CDs:</p>

   <ul>
     <li>A instalação a partir de um conjunto de CDs é mais direta.</li>
     <li>Você pode instalar em máquinas sem conexão com a Internet.</li>
         <li>Você pode instalar o Debian (em quantas máquinas você quiser) sem
         ter que baixar todos os pacotes por conta própria.</li>
     <li>O CD pode ser usado para recuperar, mais facilmente, um sistema
         Debian danificado.</li>
   </ul>

   <h2><a href="pre-installed">Compre um computador com Debian
     pré-instalado</a></h2>
   <p>Há algumas vantagens nisso:</p>
   <ul>
    <li>Você não tem que instalar o Debian.</li>
    <li>A instalação é pré-configurada para seu hardware.</li>
    <li>O fornecedor pode prover suporte técnico.</li>
   </ul>
  </div>
  <div class="item col50 lastcol">
    <h2><a href="../CD/live/">Experimente o Debian Live antes de instalar</a></h2>
    <p>
	Você pode experimentar o Debian inicializando um sistema Live a partir de um
	CD, DVD ou pendrive USB sem instalar nenhum arquivo no computador. Quando
	você estiver pronto, você pode executar o instalador incluído (a partir
	do Debian 10 Buster, ele é o amigável ao usuário final
	<a href="https://calamares.io">instalador Calamares</a>).
	Desde que as imagens atendam aos seus requisitos de tamanho, idioma e
	seleção de pacotes, este método pode ser adequado para você.
	Leia mais <a href="../CD/live#choose_live">informações sobre este método</a>
	para ajudar você a decidir.
    </p>
    <ul class="quicklist downlist">
      <li><a title="Baixe torrents Live para PC Intel e AMD de 64 bits"
	     href="<live-images-url/>/amd64/bt-hybrid/">torrents Live para PC de 64 bits</a></li>
    </ul>
  </div>
</div>
