#use wml::debian::translation-check translation="6547bb7720bfba1c2481b95c44642a4a6d3df030"
<define-tag pagetitle>Debian 10 atualizado: 10.4 liberado</define-tag>
<define-tag release_date>2020-05-09</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.4</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>O projeto Debian tem o prazer de anunciar a quarta atualização da
versão Debian estável (stable) <release> (codename <q><codename></q>).
Essa atualização pontual adiciona principalmente correções para problemas
de segurança, juntamente com alguns ajustes para problemas sérios.
Os avisos de segurança
já foram publicados separadamente e são referenciados quando disponíveis.</p>

<p>Por favor, note que este lançamento pontual não constitui uma nova versão do
Debian <release>, mas apenas atualiza alguns dos pacotes inclusos. Não há
necessidade de substituir a mídia <q><codename></q>. Após sua instalação,
os pacotes podem ser atualizados para a versão mais recente utilizando um
espelho Debian atualizado.</p>

<p>Para aqueles(as) que instalam atualizações frequentemente de
security.debian.org, não terão que atualizar muitos pacotes, e a maioria das
atualizações estão incluídas no lançamento pontual.</p>

<p>Novas imagens de instalação estarão disponíveis em breve nos repositórios
oficiais.</p>

<p>A atualização de uma instalação existente para esta nova revisão pode
ser realizada apontando o sistema de gerenciamento de pacotes para um dos vários
espelhos HTTP do Debian existentes.
Uma lista de espelhos está disponível em:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Correções de bugs gerais</h2>

<p>Esta atualização da estável (stable) adiciona algumas correções importantes
para os seguintes pacotes:</p>

<table border=0>
<tr><th>Package</th>               <th>Reason</th></tr>
<correction apt-cacher-ng "Obriga chamada segura para o servidor no acionamento do trabalho de manutenção [CVE-2020-5202]; permite compressão .zst para tarballs; aumenta o tamanho do buffer da linha de descompressão para leitura do arquivo de configuração">
<correction backuppc "Passa o nome de usuário(a) para recarregar o start-stop-daemon, prevenindo falhas de recarregamento">
<correction base-files "Atualiza para o lançamento atual">
<correction brltty "Reduz a gravidade do registro de mensagem de log evitando a geração de muitas mensagens quando usar novas versões do Orca">
<correction checkstyle "Corrige problema de injeção de Entidade Externa XML [CVE-2019-9658 CVE-2019-10782]">
<correction choose-mirror "Atualiza lista de espelhos incluídos">
<correction clamav "Novo lançamento do upstream [CVE-2020-3123]">
<correction corosync "totemsrp: Reduz MTU para evitar a geração de pacotes superdimensionados">
<correction corosync-qdevice "Corrige a inicialização do serviço">
<correction csync2 "Comando fail HELLO quando SSL é requerido">
<correction cups "Corrige estouro de buffer de heap [CVE-2020-3898] e <q>a função `ippReadIO` pode sub-ler um campo de extensão</q> [CVE-2019-8842]">
<correction dav4tbsync "Nova atualização do upstream, restaurando a compatibilidade com Thunderbird mais recente">
<correction debian-edu-config "Adiciona arquivos de política para Firefox ESR e Thunderbird para corrigir configuração de TLS/SSL">
<correction debian-installer "Atualiza o kernel ABI para 4.19.0-9">
<correction debian-installer-netboot-images "Reconstruir com atualizações propostas">
<correction debian-security-support "Nova atualização estável do upstream; atualiza o status de vários pacotes; usa <q>runuser</q> em vez de <q>su</q>">
<correction distro-info-data "Adiciona Ubuntu 20.10, e provável data de encerramento do suporte para stretch">
<correction dojo "Corrige o uso impróprio de expressões regulares [CVE-2019-10785]">
<correction dpdk "Nova atualização estável do upstream">
<correction dtv-scan-tables "Nova captura do upstream; adiciona todos os muxes DVB-T2 alemães e o satélite Eutelsat-5-West-A">
<correction eas4tbsync "Nova atualização do upstream, restaura a compatibilidade com as versões mais atuais do Thunderbird">
<correction edk2 "Correções de segurança [CVE-2019-14558 CVE-2019-14559 CVE-2019-14563 CVE-2019-14575 CVE-2019-14586 CVE-2019-14587]">
<correction el-api "Corrige atualizações do stretch para buster envolvendo Tomcat 8">
<correction fex "Corrige um potencial problema de segurança em fexsrv">
<correction filezilla "Corrige vulnerabilidade de pesquisa de caminho não confiável [CVE-2019-5429]">
<correction frr "Corrige a capacidade de próximo salto estendida">
<correction fuse "Remove comandos udevadm desatualizados dos scripts pós-instalação; não remove explicitamente fuse.conf na remoção">
<correction fuse3 "Remove comandos udevadm desatualizados dos scripts pós-instalação; não remove explicitamete fuse.conf na remoção; corrige vazamento de memória em fuse_session_new()">
<correction golang-github-prometheus-common "Estende a validade dos certificados de teste">
<correction gosa "Substituir (des)serializar com json_encode/json_decode para mitigar injeção em objeto PHP [CVE-2019-14466]">
<correction hbci4java "Auxilia a diretiva EU em serviços de pagamento (PSD2)">
<correction hibiscus "Auxilia a diretiva EU em serviços de pagamento (PSD2)">
<correction iputils "Corrige um problema no qual ping acarretaria em erro inesperado com código de falha quando houvesse endereços não testados ainda disponíveis na chamada de retorno da biblioteca getaddrinfo()">
<correction ircd-hybrid "Usa dhparam.pem para evitar quebra na inicialização">
<correction jekyll "Permite o uso de ruby-i18n 0.x e 1.x">
<correction jsp-api "Correção de atualização do stretch para buster envolvendo o Tomcat 8">
<correction lemonldap-ng "Previne acesso indesejado ao terminal de administração [CVE-2019-19791]; Corrige o plugin GrantSession que não proibia o acesso quando a autenticação em dois passos era usada; Corrige redirecionamentos arbitrários com OIDC se redirect_uri não for usado">
<correction libdatetime-timezone-perl "Atualiza dados incluídos">
<correction libreoffice "Corrige transições de slide OpenGL">
<correction libssh "Corrige possível problema de negação de serviço ao lidar com chaves AES-CTRs com OpenSSL [CVE-2020-1730]">
<correction libvncserver "Corrige estouro de pilha [CVE-2019-15690]">
<correction linux "Nova atualização estável do upstream">
<correction linux-latest "Atualiza o kernel ABI para 4.19.0-9">
<correction linux-signed-amd64 "Nova atualização estável do upstream">
<correction linux-signed-arm64 "Nova atualização estável do upstream">
<correction linux-signed-i386 "Nova atualização estável do upstream">
<correction lwip "Corrige estouro de buffer [CVE-2020-8597]">
<correction lxc-templates "Nova atualização estável do upstream; lida com linguagens que são codificadas apenas em UTF-8">
<correction manila "Corrige falta de verificação de permissões de acesso [CVE-2020-9543]">
<correction megatools "Adiciona suporte para o novo formato de links do mega.nz">
<correction mew "Corrige verificação de validade do certificado do servidor">
<correction mew-beta "Corrige verificação de validade do certificado SSL do servidor[<0;20;15M">
<correction mkvtoolnix "Reconstrói para estreitar da dependência da libmatroska6v5">
<correction ncbi-blast+ "Desabilita o suporte a SSE4.2">
<correction node-anymatch "Remove dependências desnecessárias">
<correction node-dot "Previne a execução de código após a poluição do protótipo [CVE-2020-8141]">
<correction node-dot-prop "Corrige poluição do protótipo [CVE-2020-8116]">
<correction node-knockout "Corrige fugas com versões antigas do Internet Explorer [CVE-2019-14862]">
<correction node-mongodb "Rejeita invalid _bsontypes [CVE-2019-2391 CVE-2020-7610]">
<correction node-yargs-parser "Corrige poluição de protótipo [CVE-2020-7608]">
<correction npm "Corrige acesso a caminho arbitrário [CVE-2019-16775 CVE-2019-16776 CVE-2019-16777]">
<correction nvidia-graphics-drivers "Nova atualização estável do upstream">
<correction nvidia-graphics-drivers-legacy-390xx "Nova atualização estável do upstream">
<correction nvidia-settings-legacy-340xx "New upstream release">
<correction oar "Reverte o comportamento stretch para armazenável::função perl dclone, corrige problemas de profundidade de recursão">
<correction opam "Prioriza mccs em vez de aspcud">
<correction openvswitch "Corrige vswitchd quando uma porta é adicionada e o controlador está desligado">
<correction orocos-kdl "Corrige conversão de string com Python 3">
<correction owfs "Remove pacotes Python 3 quebrados">
<correction pango1.0 "Corrige falha no pango_fc_font_key_get_variations() quando a chave é nula">
<correction pgcli "Adiciona dependências perdidas em python3-pkg-resources">
<correction php-horde-data "Fix authenticated remote code execution vulnerability [CVE-2020-8518]">
<correction php-horde-form "Corrige vulnerabilidade de execução remota de código autenticado [CVE-2020-8866]">
<correction php-horde-trean "Corrige vulnerabilidade de execução remota de código autenticado [CVE-2020-8865]">
<correction postfix "Nova atualização estável do upstream; corrige o pânico com a configuração multi-Milter durante MAIL DE; corrige as mudanças de execução d/init.d então funciona novamente com multi-instance">
<correction proftpd-dfsg "Corrige problema de acesso à memória no código keyboard-interative em mod_sftp; lida corretamente com mensagens DEBUG, IGNORE, DISCONNECT, and UNIMPLEMENTED no modo keyboard-interactive">
<correction puma "Corrige problema de Negação de Serviço [CVE-2019-16770]">
<correction purple-discord "Corrige travamentos em ssl_nss_read">
<correction python-oslo.utils "Corrige vazamento de informações confidenciais por meio de registros mistral [CVE-2019-3866]">
<correction rails "Corrige possíveis scripts entre sites por meio do auxílio de escape javascript [CVE-2020-5267]">
<correction rake "Corrige vulnerabilidades de injeção em comandos [CVE-2020-8130]">
<correction raspi3-firmware "Corrige incompatibilidade de nomes dtb em z50-raspi-firmware; Corrige boot no Raspberry Pi família 1 e 0">
<correction resource-agents "Corrige <q>monitor eth que não lista interfaces sem endereço de IP atribuído</q>; remove o patch desnecessário xen-toolstack; corrige o uso não-padrão do agente ZFS">
<correction rootskel "Desabilita suporte a múltiplos consoles se o anterior estiver em uso">
<correction ruby-i18n "Corrige geração de gemspec">
<correction rubygems-integration "Evita avisos de depreciação quando usuários(as) instalam a nova versão do Rubygems via <q>gem update --system</q>">
<correction schleuder "Melhora o patch para lidar com erros de codificação introduzidos na versão anterior; muda a codificação padrão para UTF-8; deixa x-add-key lidar com e-mails com chaves codificadas para impressão em anexo; corrige x-attach-listkey com e-mails criados no Thunderbird que incluem cabeçalhos protegidos">
<correction scilab "Corrige biblioteca carregadas com OpenJDK 11.0.7">
<correction serverspec-runner "Suporte Ruby 2.5">
<correction softflowd "Corrige agregação de fluxo interrompida que pode resultar em estouro de tabela de fluxo e uso de CPU de 100%">
<correction speech-dispatcher "Corrige latência padrão do pulseaudio que ativa a emissão de <q>ruídos</q>">
<correction spl-linux "Corrige impasse">
<correction sssd "Corrige sssd_be busy-looping quando a conexão LDAP está intermitente">
<correction systemd "Quando autorizado por kit de políticas, resolve novamente o retorno de chamada/dados de usuários(as) em vez de armazená-lo [CVE-2020-1712]; instala 60-block.rules em udev-udeb e initramfs-tools">
<correction taglib "Corrige problemas com arquivos OGG corrompidos">
<correction tbsync "Nova atualização do upstream, restaura compatibilidade com versão recente do Thunderbird">
<correction timeshift "Corrige o uso previsível de diretório temporário [CVE-2020-10174]">
<correction tinyproxy "Apenas define PIDDIR, se PIDFILE for uma string de cumprimento diferente de zero">
<correction tzdata "Nova atualização estável do upstream">
<correction uim "Cancela registro de módulos que não estejam instalados, corrigindo a regressão no upload anterior">
<correction user-mode-linux "Corrige falhas de compilação em kernels estáveis atuais">
<correction vite "Corrige falha quando houver mais de 32 elementos">
<correction waagent "Nova atualização do upstream; suporte a coinstalação com cloud-init">
<correction websocket-api "Corrige atualizações do stretch para buster envolvendo Tomcat 8">
<correction wpa "Não tenta detectar incompatibilidade do PSK durante a redocificação do PTK; verifica por suporte FT ao selecionar as suítes FT; corrige problema de randomização MAC com alguns cartões">
<correction xdg-utils "xdg-open: corrige a verificação pcmanfm e lida com diretórios com espaços em seus nomes; xdg-screensaver: limpa o nome da janela antes de enviá-lo pelo D-Bus; xdg-mime: Cria diretório config se ele ainda não existir">
<correction xtrlock "Corrige bloqueio de (alguns) dispositivos multitoque enquanto estão bloqueados [CVE-2016-10894]">
<correction zfs-linux "Corrige potenciais problemas de impasse">
</table>


<h2>Atualizações de segurança</h2>


<p>Essa revisão adiciona as seguintes atualizações de segurança do lançamento
estável (stable).
A equipe de segurança já lançou um comunicado para cada uma dessas atualizações:</p>

<table border=0>
<tr><th>ID Consultivo</th> <th>Pacote</th></tr>
<dsa 2020 4616 qemu>
<dsa 2020 4617 qtbase-opensource-src>
<dsa 2020 4618 libexif>
<dsa 2020 4619 libxmlrpc3-java>
<dsa 2020 4620 firefox-esr>
<dsa 2020 4623 postgresql-11>
<dsa 2020 4624 evince>
<dsa 2020 4625 thunderbird>
<dsa 2020 4627 webkit2gtk>
<dsa 2020 4629 python-django>
<dsa 2020 4630 python-pysaml2>
<dsa 2020 4631 pillow>
<dsa 2020 4632 ppp>
<dsa 2020 4633 curl>
<dsa 2020 4634 opensmtpd>
<dsa 2020 4635 proftpd-dfsg>
<dsa 2020 4636 python-bleach>
<dsa 2020 4637 network-manager-ssh>
<dsa 2020 4638 chromium>
<dsa 2020 4639 firefox-esr>
<dsa 2020 4640 graphicsmagick>
<dsa 2020 4641 webkit2gtk>
<dsa 2020 4642 thunderbird>
<dsa 2020 4643 python-bleach>
<dsa 2020 4644 tor>
<dsa 2020 4645 chromium>
<dsa 2020 4646 icu>
<dsa 2020 4647 bluez>
<dsa 2020 4648 libpam-krb5>
<dsa 2020 4649 haproxy>
<dsa 2020 4650 qbittorrent>
<dsa 2020 4651 mediawiki>
<dsa 2020 4652 gnutls28>
<dsa 2020 4653 firefox-esr>
<dsa 2020 4654 chromium>
<dsa 2020 4655 firefox-esr>
<dsa 2020 4656 thunderbird>
<dsa 2020 4657 git>
<dsa 2020 4658 webkit2gtk>
<dsa 2020 4659 git>
<dsa 2020 4660 awl>
<dsa 2020 4661 openssl>
<dsa 2020 4663 python-reportlab>
<dsa 2020 4664 mailman>
<dsa 2020 4665 qemu>
<dsa 2020 4666 openldap>
<dsa 2020 4667 linux-signed-amd64>
<dsa 2020 4667 linux-signed-arm64>
<dsa 2020 4667 linux-signed-i386>
<dsa 2020 4667 linux>
<dsa 2020 4669 nodejs>
<dsa 2020 4671 vlc>
<dsa 2020 4672 trafficserver>
</table>


<h2>Pacotes removidos</h2>

<p>Os seguintes pacotes foram removidos devido circunstâncias além do nosso controle:</p>

<table border=0>
<tr><th>Pacote</th>               <th>Motivo</th></tr>
<correction getlive "Quebrado devido a alterações do Hotmail">
<correction gplaycli "Quebrado devido a alterações na API do Google">
<correction kerneloops "Serviço do(a) upstream não está mais disponível">
<correction lambda-align2 "[arm64 armel armhf i386 mips64el ppc64el s390x] quebrado em arquiteturas não amd64">
<correction libmicrodns "Questões de segurança">
<correction libperlspeak-perl "Questões de segurança; sem manutenção">
<correction quotecolors "Incompatibilidade com versões recentes do Thunderbird">
<correction torbirdy "Incompatibilidade com versões recentes do Thunderbird">
<correction ugene "Não livre; falha na compilação">
<correction yahoo2mbox "Quebrado por muitos anos">

</table>

<h2>Debian Installer</h2>
<p>O instalador foi atualizado para incluir correções incorporadas na estável
(stable) pelo lançamento pontual.</p>

<h2>URLs</h2>

<p>A lista completa de pacotes que sofreram mudanças com essa revisão:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>A versão estável (stable) atual:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Atualizações propostas (proposed updates) para a versão estável (stable):</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Informação da versão estável (notas de lançamento, errata, etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Anúncios e informações de segurança:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Sobre o Debian</h2>

<p>O Projeto Debian é uma associação de desenvolvedores(as) de software livre
que voluntariamente doam seu tempo e esforço para produzir o sistema
operacional completamente livre Debian.</p>

<h2>Informações para contato</h2>

<p>Para mais informações, por favor visite a página do Debian em
<a href="$(HOME)/">https://www.debian.org/</a>, ou envie um e-mail (em inglês)
para &lt;press@debian.org&gt;, ou entre em contato com a equipe de lançamento
da estável (stable) em &lt;debian-release@lists.debian.org&gt;.</p>
