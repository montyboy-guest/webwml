#use wml::debian::template title="Developer Locations" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="bd2e76d96db915ccd0dee367a373417db7422565" maintainer="galaxico"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Where are the Debian Developers (DD) located? If a DD has specified the home coordinates in the developer database, it's visible in our World Map.</p>
</aside>

<p>
The map below was generated from an anonymized <a
href="developers.coords">list of developer coordinates</a> using the
program <a href="https://packages.debian.org/stable/graphics/xplanet">
xplanet</a>.
</p>

<img src="developers.map.jpeg" alt="World Map">

<h2>How to add your Coordinates</h2>

<p>
If you would like to add your coordinates to your database entry, log in
to the <a href="https://db.debian.org">Debian Developers' Database</a>
and modify your entry. If you don't know the coordinates of your home
town, you can use <a href="https://www.openstreetmap.org">OpenStreetMap</a> to look
them up. Search for your city and select the direction arrows next to the
search field. Drag the green marker to the OSM map, and the coordinates
will appear in the <em>From</em> field.
</p>

<p>The format for coordinates is one of the following:</p>

<dl>
  <dt>Decimal Degrees</dt>
    <dd>The format is <code>+-DDD.DDDDDDDDDDDDDDD</code>. Programs like
    Xearth and many other positioning web sites use it. The precision is
    limited to 4 or 5 decimals.</dd>
  <dt>Degrees Minutes (DGM)</dt>
    <dd>The format is <code>+-DDDMM.MMMMMMMMMMMMM</code>. It's not
    arithmetic, but a packed representation of two separate units: degrees
    and minutes. This output is common with some types of handheld GPS
    devices and NMEA format GPS messages.</dd>
  <dt>Degrees Minutes Seconds (DGMS)</dt>
    <dd>The format is <code>+-DDDMMSS.SSSSSSSSSSS</code>. Like DGM, it's
    not arithmetic, but a packed representation of three separate units:
    degrees, minutes, and seconds. This output is typically derived
    from web sites which give 3 values for each position. For example,
    <code>34:50:12.24523 North</code> might be the given position, and in
    DGMS it would be <code>+0345012.24523</code>.</dd>
</dl>

<p>
<strong>Please note:</strong> <code>+</code> is North for latitude,
<code>+</code> is East for longitude. It's important to specify enough
leading zeros to dis-ambiguate the  format being used if your position
is less than 2 degrees from a zero point.
</p>
