#use wml::debian::template title="Reasons to use Debian" MAINPAGE="true"
#include "$(ENGLISHDIR)/releases/info"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#users">Debian for Users</a></li>
    <li><a href="#devel">Debian for Developers</a></li>
    <li><a href="#enterprise">Debian for Enterprise Environments</a></li>
  </ul>
</div>

<p>
There are a lot of reasons to choose Debian as your operating system –
as a user, as a developer, and even in enterprise environments. Most
users appreciate the stability, and the smooth upgrade processes
of both packages and the entire distribution. Debian is also widely
used by software and hardware developers because it runs on numerous
architectures and devices, offers a public bug tracker and other tools
for developers. If you plan to use Debian in a professional environment,
there are additional benefits like LTS versions and cloud images.
</p>

<aside>
<p><span class="far fa-comment-dots fa-3x"></span> For me it's the perfect level of ease of use and stability. I've used various different distributions over the years but Debian is the only one that just works. <a href="https://www.reddit.com/r/debian/comments/8r6d0o/why_debian/e0osmxx">NorhamsFinest on Reddit</a></p>
</aside>

<h2><a id="users">Debian for Users</a></h2>

<dl>
  <dt><strong>Debian is Free Software.</strong></dt>
  <dd>
    Debian is made of free and open source software and will always be 100%
    <a href="free">free</a>. Free for anyone to use, modify, and
    distribute. This is our main promise to <a href="../users">our users</a>. It's also free of cost.
  </dd>
</dl>

<dl>
  <dt><strong>Debian is stable and secure.</strong></dt>
  <dd>
    Debian is a Linux-based operating system for a wide range of devices including
    laptops, desktops and servers. We provide a reasonable default configuration for
    every package as well as regular security updates during the packages' lifetimes.
  </dd>
</dl>

<dl>
  <dt><strong>Debian has extensive Hardware Support.</strong></dt>
  <dd>
    Most hardware is supported by the Linux kernel which means that
    Debian will support it as well. Proprietary drivers for hardware
    are available if necessary.
  </dd>
</dl>

<dl>
  <dt><strong>Debian offers a flexible Installer.</strong></dt>
  <dd>
    Our <a
    href="https://cdimage.debian.org/images/release/current-live/amd64/iso-hybrid/">Live
    CD</a> is for everyone who wants to give Debian a try before
    installing it. It also includes the Calamares installer which makes
    it easy to install Debian from the live system. More experienced
    users can use the Debian installer with more options for fine-tuning,
    including the possibility to use an automated network installation
    tool.
  </dd>
</dl>

<dl>
  <dt><strong>Debian provides smooth Upgrades.</strong></dt>
  <dd>
    It's easy to keep our operating system up-to-date, whether you want
    to upgrade to a completely new release or just update a single package.
  </dd>
</dl>

<dl>
  <dt><strong>Debian is the Base for many other Distributions.</strong></dt>
  <dd>
    Many popular Linux distributions, like Ubuntu, Knoppix, PureOS or Tails, 
    are based on Debian. We provide all the tools so that everyone can 
    extend the software packages from the Debian archive with their own 
    packages if needed.
  </dd>
</dl>

<dl>
  <dt><strong>The Debian Project is a Community.</strong></dt>
  <dd>
    Everyone can be a part of our community; you don't
    have to be a developer or sysadmin. Debian has a
    <a href="../devel/constitution">democratic governance
    structure</a>. Since all members of the Debian project have equal
    rights, Debian cannot be controlled by a single company. Our
    developers are from more than 60 different countries, and Debian
    itself is translated into more than 80 languages.
  </dd>
</dl>

<aside>
<p><span class="far fa-comment-dots fa-3x"></span> The reason behind Debian’s status as a developer’s operating system is the large number of packages and software support, which are important for developers. It’s highly recommended for advanced programmers and system administrators. <a href="https://fossbytes.com/best-linux-distros-for-programming-developers/">Adarsh Verma on Fossbytes</a></p>
</aside>

<h2><a id="devel">Debian for Developers</a></h2>

<dl>
  <dt><strong>Multiple Hardware Architectures</strong></dt>
  <dd>
    Debian supports a <a href="../ports">long list</a> of CPU architectures,
    including amd64, i386, multiple versions of ARM and MIPS, POWER7, POWER8,
    IBM System z and RISC-V. Debian is also available for niche architectures.
  </dd>
</dl>

<dl>
  <dt><strong>IoT and Embedded Devices</strong></dt>
  <dd>
    Debian runs on a wide range of devices, like the Raspberry Pi,
    variants of QNAP, mobile devices, home routers and a lot of Single
    Board Computers (SBC).
  </dd>
</dl>

<dl>
  <dt><strong>Huge Number of Software Packages</strong></dt>
  <dd>
    Debian has a large number of <a
    href="$(DISTRIB)/packages">packages</a> (currently in
    stable: <packages_in_stable> packages) which use the <a
    href="https://manpages.debian.org/unstable/dpkg-dev/deb.5.en.html">deb
    format</a>.
  </dd>
</dl>

<dl>
  <dt><strong>Different Releases</strong></dt>
  <dd>
    Besides our stable release, you can install newer software versions
    by using the testing or unstable releases.
  </dd>
</dl>

<dl>
  <dt><strong>Public Bug Tracker</strong></dt>
  <dd>
    Our Debian <a href="../Bugs">bug tracking system</a> (BTS) is publicly
    available for everybody via a web browser. We do not hide our software
    bugs, and you can easily submit new bug reports or join the discussion.
  </dd>
</dl>

<dl>
  <dt><strong>Debian Policy and Developer Tools</strong></dt>
  <dd>
    Debian offers high-quality software. To learn more about our
    standards, read the <a href="../doc/debian-policy/">policy</a>
    which defines technical requirements for every package included
    in the distribution. Our Continuous Integration strategy involves
    Autopkgtest (runs tests on packages), Piuparts (tests installation,
    upgrade and removal), and Lintian (checks packages for inconsistencies
    and errors).
  </dd>
</dl> 

<aside>
<p><span class="far fa-comment-dots fa-3x"></span> Stability is synonym with Debian. [...]  Security is one of the most important Debian features. <a href="https://www.pontikis.net/blog/five-reasons-to-use-debian-as-a-server">Christos Pontikis on pontikis.net</a></p>
</aside>

<h2><a id="enterprise">Debian for Enterprise Environments</a></h2>

<dl>
  <dt><strong>Debian is reliable.</strong></dt>
  <dd>
    Debian proves its reliability every day in thousands of real world
    scenarios, ranging from single user laptops to super-colliders, stock
    exchanges and the automotive industry. It's also popular in the academic
    world, in science and in the public sector.
  </dd>
</dl>

<dl>
  <dt><strong>Debian has many Experts.</strong></dt>
  <dd>
    Our package maintainers do not only take care of the Debian packaging
    and incorporating new upstream versions. Often they're experts
    on the application itself and therefore contribute to upstream
    development directly.
  </dd>
</dl>

<dl>
  <dt><strong>Debian is secure.</strong></dt>
  <dd>
    Debian offers security support for its stable releases. Many other
    distributions and security researchers rely on Debian's security tracker.
  </dd>
</dl>

<dl>
  <dt><strong>Long Term Support</strong></dt>
  <dd>
    Debian's free of charge <a href="https://wiki.debian.org/LTS">Long Term Support</a>
    (LTS) version extends the lifetime of all Debian
    stable releases to at least 5 years. Additionally, the commercial
    <a href="https://wiki.debian.org/LTS/Extended">Extended LTS</a>
    initiative supports a limited set of packages for more than 5 years.
  </dd>
</dl>

<dl>
  <dt><strong>Cloud Images</strong></dt>
  <dd>
    Official cloud images are available for all major cloud platforms. We also
    provide the tools and configuration so you can build your own customized
    cloud images. You can also use Debian in virtual machines on the desktop or
    in a container.
  </dd>
</dl>
