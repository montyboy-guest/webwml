<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>In Eclipse Web Tools Platform, a component of the Eclipse IDE, XML and
DTD files referring to external entities could be exploited to send the
contents of local files to a remote server when edited or validated,
even when external entity resolution is disabled in the user
preferences.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
3.6.3-3+deb9u1.</p>

<p>We recommend that you upgrade your eclipse-wtp packages.</p>

<p>For the detailed security status of eclipse-wtp please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/eclipse-wtp">https://security-tracker.debian.org/tracker/eclipse-wtp</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2404.data"
# $Id: $
