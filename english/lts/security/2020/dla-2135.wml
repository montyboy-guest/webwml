<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>The following CVEs were reported for jackson-databind source package.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9546">CVE-2020-9546</a>

    <p>FasterXML jackson-databind 2.x before 2.9.10.4 mishandles the
    interaction between serialization gadgets and typing, related
    to org.apache.hadoop.shaded.com.zaxxer.hikari.HikariConfig
    (aka shaded hikari-config).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9547">CVE-2020-9547</a>

    <p>FasterXML jackson-databind 2.x before 2.9.10.4 mishandles the
    interaction between serialization gadgets and typing, related
    to com.ibatis.sqlmap.engine.transaction.jta.JtaTransactionConfig
    (aka ibatis-sqlmap).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9548">CVE-2020-9548</a>

     <p>FasterXML jackson-databind 2.x before 2.9.10.4 mishandles the
     interaction between serialization gadgets and typing, related
     to br.com.anteros.dbcp.AnterosDBCPConfig (aka anteros-core).</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.4.2-2+deb8u12.</p>

<p>We recommend that you upgrade your jackson-databind packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2135.data"
# $Id: $
