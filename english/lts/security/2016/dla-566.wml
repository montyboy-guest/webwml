<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>CakePHP, an open-source web application framework for PHP, was
vulnerable to SSRF (Server Side
Request Forgery) attacks. Remote attacker can utilize it for at least
DoS (Denial of Service) attacks, if the target application accepts XML
as an input. It is caused by insecure design of Cake's Xml class.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.3.15-1+deb7u1.</p>

<p>We recommend that you upgrade your cakephp packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-566.data"
# $Id: $
