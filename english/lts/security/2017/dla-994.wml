<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5974">CVE-2017-5974</a>

      <p>Heap-based buffer overflow in the __zzip_get32 function in fetch.c
      in zziplib allows remote attackers to cause a denial of service
      (crash) via a crafted ZIP file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5975">CVE-2017-5975</a>

      <p>Heap-based buffer overflow in the __zzip_get64 function in fetch.c
      in zziplib allows remote attackers to cause a denial of service
      (crash) via a crafted ZIP file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5976">CVE-2017-5976</a>

      <p>Heap-based buffer overflow in the zzip_mem_entry_extra_block
      function in memdisk.c in zziplib allows remote attackers to cause
      a denial of service (crash) via a crafted ZIP file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5978">CVE-2017-5978</a>

      <p>The zzip_mem_entry_new function in memdisk.c in zziplib allows
      remote attackers to cause a denial of service (out-of-bounds
      read and crash) via a crafted ZIP file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5979">CVE-2017-5979</a>

      <p>The prescan_entry function in fseeko.c in zziplib allows remote
      attackers to cause a denial of service (NULL pointer dereference
      and crash) via a crafted ZIP file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5980">CVE-2017-5980</a>

      <p>The zzip_mem_entry_new function in memdisk.c in zziplib allows
      remote attackers to cause a denial of service (NULL pointer
      dereference and crash) via a crafted ZIP file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5981">CVE-2017-5981</a>

      <p>seeko.c in zziplib allows remote attackers to cause a denial of
      service (assertion failure and crash) via a crafted ZIP file.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.13.56-1.1+deb7u1.</p>

<p>We recommend that you upgrade your zziplib packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-994.data"
# $Id: $
