<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues have been found in the JBIG2 decoder library,
which may lead to lead to denial of service or the execution of arbitrary
code if a malformed image file (usually embedded in a PDF document) is
opened.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.13-4~deb7u1.</p>

<p>For the stable distribution (jessie), this problem has been fixed in
version 0.13-4~deb8u1.</p>

<p>For the upcoming stable distribution (stretch) and for the unstable
distribution (sid), this problem has been fixed in version 0.13-4.</p>

<p>We recommend that you upgrade your jbig2dec packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-874.data"
# $Id: $
