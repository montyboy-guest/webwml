<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two memory handling issues were found in gst-plugins-good0.10:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10198">CVE-2016-10198</a>

    <p>An invalid read can be triggered in the aacparse element via a
    maliciously crafted file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5840">CVE-2017-5840</a>

    <p>An out of bounds heap read can be triggered in the qtdemux element
    via a maliciously crafted file.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.10.31-3+nmu1+deb7u2.</p>

<p>We recommend that you upgrade your gst-plugins-good0.10 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-828.data"
# $Id: $
