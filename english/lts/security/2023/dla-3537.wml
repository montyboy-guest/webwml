<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This update ships updated CPU microcode for some types of Intel CPUs
and provides mitigations for security vulnerabilities.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-40982">CVE-2022-40982</a>

    <p>Daniel Moghimi discovered Gather Data Sampling (GDS), a hardware
    vulnerability which allows unprivileged speculative access to data
    which was previously stored in vector registers.</p>

    <p>For details please refer to <a href="https://downfall.page/">https://downfall.page/</a> and
    <a href="https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/technical-documentation/gather-data-sampling.html.">https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/technical-documentation/gather-data-sampling.html.</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41804">CVE-2022-41804</a>

    <p>Unauthorized error injection in Intel SGX or Intel TDX for some
    Intel Xeon Processors which may allow a local user to potentially
    escalate privileges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-23908">CVE-2023-23908</a>

    <p>Improper access control in some 3rd Generation Intel Xeon Scalable
    processors may result in an information leak.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
3.20230808.1~deb10u1.</p>

<p>We recommend that you upgrade your intel-microcode packages.</p>

<p>For the detailed security status of intel-microcode please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/intel-microcode">https://security-tracker.debian.org/tracker/intel-microcode</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3537.data"
# $Id: $
