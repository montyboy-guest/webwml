<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Latest MariaDB minor maintenance release 10.3.39 included a fix for the
following security vulnerability:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-47015">CVE-2022-47015</a>

    <p>Spider storage engine vulnerable to Denial of Service</p></li>

</ul>

<p>For Debian 10 buster, this problem has been fixed in version
1:10.3.39-0+deb10u1.</p>

<p>Additionally the backwards incompatible libmariadb API change has
been reverted (Closes: #1031773).</p>

<p>We recommend that you upgrade your mariadb-10.3 packages.</p>

<p>For the detailed security status of mariadb-10.3 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/mariadb-10.3">https://security-tracker.debian.org/tracker/mariadb-10.3</a></p>

<p>Note! According to <a href="https://mariadb.org/about/#maintenance-policy">https://mariadb.org/about/#maintenance-policy</a> this
was the last minor maintenance release for MariaDB 10.3 series.</p>


<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3444.data"
# $Id: $
