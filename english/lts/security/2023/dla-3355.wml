<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two important bugs were discovered in xapian-core, a search engine
library, that led to potential database corruption on disk full, and
incorrectly reporting corruption for a database with replication
changesets.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1.4.11-1+deb10u1.</p>

<p>We recommend that you upgrade your xapian-core packages.</p>

<p>For the detailed security status of xapian-core please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/xapian-core">https://security-tracker.debian.org/tracker/xapian-core</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3355.data"
# $Id: $
