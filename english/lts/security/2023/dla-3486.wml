<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The source package ocsinventory-server has been updated to address the
API change in php-cas due to <a href="https://security-tracker.debian.org/tracker/CVE-2022-39369">CVE-2022-39369</a>, see DLA 3485-1 for details.</p>

<p>CAS is an optional authentication mechanism in the binary package
ocsinventory-reports, and if used, ocsinventory-reports will stop
working until it has been reconfigured:</p>

<p>It now requires the baseURL of to-be-authenticated service to be
configured.</p>

<p>For ocsinventory-reports, this is configured with the variable
$cas_service_base_url in the file
/usr/share/ocsinventory-reports/backend/require/cas.config.php</p>

<p>Warning: regardless of this update, ocsreports-server should only be
used in secure and trusted environments.</p>


<p>For Debian 10 buster, this update is available through version
2.5+dfsg1-1+deb10u1.</p>

<p>We recommend that you upgrade your ocsinventory-server packages.</p>

<p>For the detailed security status of ocsinventory-server please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ocsinventory-server">https://security-tracker.debian.org/tracker/ocsinventory-server</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3486.data"
# $Id: $
