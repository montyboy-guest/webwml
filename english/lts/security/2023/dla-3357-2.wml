<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The previous Imagemagick security update caused a regression in some
perl packages due to overly restrictive hardening in a policy update
(reading from /etc/ was forbidden). This hardening patch has been
removed.</p>

<p>For Debian 10 buster, this problem has been fixed in version
8:6.9.10.23+dfsg-2.1+deb10u4.</p>

<p>We recommend that you upgrade your imagemagick packages.</p>

<p>For the detailed security status of imagemagick please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/imagemagick">https://security-tracker.debian.org/tracker/imagemagick</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3357-2.data"
# $Id: $
