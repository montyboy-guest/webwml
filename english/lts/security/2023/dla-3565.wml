<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were discovered in Loofah, a Ruby library for
HTML/XML transformation and sanitization. An attacker could launch
cross-site scripting (XSS) and denial-of-service (DoS) attacks through
crafted HTML/XML documents.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23514">CVE-2022-23514</a>

    <p>Inefficient regular expression that is susceptible to excessive
    backtracking when attempting to sanitize certain SVG
    attributes. This may lead to a denial of service through CPU
    resource consumption.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23515">CVE-2022-23515</a>

    <p>Cross-site scripting via the image/svg+xml media type in data
    URIs.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23516">CVE-2022-23516</a>

    <p>Loofah uses recursion for sanitizing CDATA sections, making it
    susceptible to stack exhaustion and raising a SystemStackError
    exception. This may lead to a denial of service through CPU
    resource consumption.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
2.2.3-1+deb10u2.</p>

<p>We recommend that you upgrade your ruby-loofah packages.</p>

<p>For the detailed security status of ruby-loofah please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ruby-loofah">https://security-tracker.debian.org/tracker/ruby-loofah</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3565.data"
# $Id: $
