<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several flaws were discovered in jackson-databind, a fast and powerful JSON
library for Java.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36518">CVE-2020-36518</a>

    <p>Java StackOverflow exception and denial of service via a large depth of
    nested objects.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42003">CVE-2022-42003</a>

    <p>In FasterXML jackson-databind resource exhaustion can occur because of a
    lack of a check in primitive value deserializers to avoid deep wrapper
    array nesting, when the UNWRAP_SINGLE_VALUE_ARRAYS feature is enabled.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42004">CVE-2022-42004</a>

    <p>In FasterXML jackson-databind resource exhaustion can occur because of a
    lack of a check in BeanDeserializerBase.deserializeFromArray to prevent use
    of deeply nested arrays. An application is vulnerable only with certain
    customized choices for deserialization.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
2.9.8-3+deb10u4.</p>

<p>We recommend that you upgrade your jackson-databind packages.</p>

<p>For the detailed security status of jackson-databind please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/jackson-databind">https://security-tracker.debian.org/tracker/jackson-databind</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3207.data"
# $Id: $
