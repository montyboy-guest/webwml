<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in mod-wsgi, a Python WSGI adapter module for
Apache. A request from an untrusted proxy does not remove the X-Client-IP
header and thus allowing this header to be passed to the target WSGI application.</p>


<p>For Debian 10 buster, this problem has been fixed in version
4.6.5-1+deb10u1.</p>

<p>We recommend that you upgrade your mod-wsgi packages.</p>

<p>For the detailed security status of mod-wsgi please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/mod-wsgi">https://security-tracker.debian.org/tracker/mod-wsgi</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3111.data"
# $Id: $
