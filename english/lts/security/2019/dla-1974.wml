<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in proftp-dfsg, a versatile, virtual-hosting FTP
daemon.</p>

<p>Due to incorrect handling of overly long commands, a remote
unauthenticated user could trigger a denial-of-service by reaching an
endless loop.</p>


<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.3.5e+r1.3.5-2+deb8u4.</p>

<p>We recommend that you upgrade your proftpd-dfsg packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1974.data"
# $Id: $
