<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was found that the fix for <a href="https://security-tracker.debian.org/tracker/CVE-2018-19758">CVE-2018-19758</a> was incomplete. That
has been addressed in this update. The description
for <a href="https://security-tracker.debian.org/tracker/CVE-2018-19758">CVE-2018-19758</a> follows:</p>

    <p>A heap-buffer-overflow vulnerability was discovered in libsndfile, the
    library for reading and writing files containing sampled sound. This flaw
    might be triggered by remote attackers to cause denial of service (out of
    bounds read and application crash).</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.0.25-9.1+deb8u4.</p>

<p>We recommend that you upgrade your libsndfile packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1712.data"
# $Id: $
