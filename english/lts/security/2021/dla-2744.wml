<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This update is merely a rebuild of the usermode package, which are
a set of graphical tools for certain user account management tasks,
fixing two issues:</p>

<p>a) the versioning issue as wheezy (Debian 7) had a greater version
   than jessie (Debian 8) and stretch (Debian 9), thereby causing
   upgrade issues.
b) the package now Depends and Build-Depends on the newer
   libuser1-dev (>= 1:0.62~dfsg-0.1) to ensure the latest
   version of libuser is used (which was a security fix).</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.109-1+deb9u1.</p>

<p>We recommend that you upgrade your usermode packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2744.data"
# $Id: $
