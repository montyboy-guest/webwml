<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities have been found in Qemu, a fast processor
emulator.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3713">CVE-2021-3713</a>

  <p>An out-of-bounds write flaw was found in the UAS (USB Attached SCSI) device
  emulation of QEMU. The device uses the guest supplied stream number
  unchecked, which can lead to out-of-bounds access to the UASDevice-&gt;data3
  and UASDevice-&gt;status3 fields. A malicious guest user could use this flaw
  to crash QEMU or potentially achieve code execution with the privileges of
  the QEMU process on the host.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3682">CVE-2021-3682</a>

  <p>A flaw was found in the USB redirector device emulation of QEMU. It occurs
  when dropping packets during a bulk transfer from a SPICE client due to the
  packet queue being full. A malicious SPICE client could use this flaw to
  make QEMU call free() with faked heap chunk metadata, resulting in a crash
  of QEMU or potential code execution with the privileges of the QEMU process
  on the host.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3527">CVE-2021-3527</a>

  <p>A flaw was found in the USB redirector device (usb-redir) of QEMU. Small
  USB packets are combined into a single, large transfer request, to reduce
  the overhead and improve performance. The combined size of the bulk
  transfer is used to dynamically allocate a variable length array (VLA) on
  the stack without proper validation. Since the total size is not bounded, a
  malicious guest could use this flaw to influence the array length and cause
  the QEMU process to perform an excessive allocation on the stack, resulting
  in a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3594">CVE-2021-3594</a>

  <p>An invalid pointer initialization issue was found in the SLiRP networking
  implementation of QEMU. The flaw exists in the udp_input() function and
  could occur while processing a udp packet that is smaller than the size of
  the <q>udphdr</q> structure. This issue may lead to out-of-bounds read access or
  indirect host memory disclosure to the guest. The highest threat from this
  vulnerability is to data confidentiality.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3592">CVE-2021-3592</a>

  <p>An invalid pointer initialization issue was found in the SLiRP networking
  implementation of QEMU. The flaw exists in the bootp_input() function and
  could occur while processing a udp packet that is smaller than the size of
  the <q>bootp_t</q> structure. A malicious guest could use this flaw to leak 10
  bytes of uninitialized heap memory from the host. The highest threat from
  this vulnerability is to data confidentiality.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3595">CVE-2021-3595</a>

  <p>An invalid pointer initialization issue was found in the SLiRP networking
  implementation of QEMU. The flaw exists in the tftp_input() function and
  could occur while processing a udp packet that is smaller than the size of
  the <q>tftp_t</q> structure. This issue may lead to out-of-bounds read access or
  indirect host memory disclosure to the guest. The highest threat from this
  vulnerability is to data confidentiality.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1:2.8+dfsg-6+deb9u15.</p>

<p>We recommend that you upgrade your qemu packages.</p>

<p>For the detailed security status of qemu please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/qemu">https://security-tracker.debian.org/tracker/qemu</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2753.data"
# $Id: $
