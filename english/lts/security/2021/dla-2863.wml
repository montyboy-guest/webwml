<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues have been found in the Mozilla Firefox
web browser, which could potentially result in the execution
of arbitrary code, information disclosure or spoofing.</p>

<p>Debian follows the extended support releases (ESR) of Firefox. Support
for the 78.x series has ended, so starting with this update we're now
following the 91.x releases.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
91.4.1esr-1~deb9u1.</p>

<p>We recommend that you upgrade your firefox-esr packages.</p>

<p>For the detailed security status of firefox-esr please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/firefox-esr">https://security-tracker.debian.org/tracker/firefox-esr</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2863.data"
# $Id: $
