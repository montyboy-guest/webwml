<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in salt, a powerful remote
execution manager, which could result in retrieve of user tokens from
the salt master, execution of arbitrary commands on salt minions,
arbitrary directory access to authenticated users or arbitrary code
execution on salt-api hosts.</p>

<p>For the oldstable distribution (stretch), these problems have been fixed
in version 2016.11.2+ds-1+deb9u3.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 2018.3.4+dfsg1-6+deb10u1.</p>

<p>We recommend that you upgrade your salt packages.</p>

<p>For the detailed security status of salt please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/salt">\
https://security-tracker.debian.org/tracker/salt</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4676.data"
# $Id: $
