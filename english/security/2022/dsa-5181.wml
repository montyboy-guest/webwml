<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in Request Tracker, an
extensible trouble-ticket tracking system.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-25802">CVE-2022-25802</a>

   <p>It was discovered that Request Tracker is vulnerable to a cross-site
   scripting (XSS) attack when displaying attachment content with
   fraudulent content types.</p></li>

</ul>

<p>Additionally it was discovered that Request Tracker did not perform full
rights checks on accesses to file or image type custom fields, possibly
allowing access to these custom fields by users without rights to access
to the associated objects, resulting in information disclosure.</p>

<p>For the oldstable distribution (buster), these problems have been fixed
in version 4.4.3-2+deb10u2.</p>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 4.4.4+dfsg-2+deb11u2.</p>

<p>We recommend that you upgrade your request-tracker4 packages.</p>

<p>For the detailed security status of request-tracker4 please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/request-tracker4">\
https://security-tracker.debian.org/tracker/request-tracker4</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5181.data"
# $Id: $
