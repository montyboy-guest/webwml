#use wml::debian::translation-check translation="4924e09e5cb1b4163d7ec7161488354e4167b24c"
<define-tag pagetitle>Debian 11 is bijgewerkt: 11.5 werd uitgebracht</define-tag>
<define-tag release_date>2022-09-10</define-tag>
#use wml::debian::news

<define-tag release>11</define-tag>
<define-tag codename>bullseye</define-tag>
<define-tag revision>11.5</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Het Debian-project kondigt met genoegen de vijfde update aan van zijn
stabiele distributie Debian <release> (codenaam <q><codename></q>).
Deze tussenrelease voegt voornamelijk correcties voor beveiligingsproblemen toe,
samen met een paar aanpassingen voor ernstige problemen. Beveiligingsadviezen
werden reeds afzonderlijk gepubliceerd en, waar beschikbaar, wordt hiernaar
verwezen.</p>

<p>Merk op dat de tussenrelease geen nieuwe versie van Debian <release> is,
maar slechts een update van enkele van de meegeleverde pakketten. Het is niet
nodig om oude media met <q><codename></q> weg te gooien. Na de installatie
kunnen pakketten worden opgewaardeerd naar de huidige versie door een
bijgewerkte Debian-spiegelserver te gebruiken.</p>

<p>Wie regelmatig updates installeert vanuit security.debian.org zal niet veel
pakketten moeten updaten, en de meeste van dergelijke updates zijn opgenomen in
de tussenrelease.</p>

<p>Nieuwe installatie-images zullen binnenkort beschikbaar zijn op de gewone
plaatsen.</p>

<p>Het upgraden van een bestaande installatie naar deze revisie kan worden
bereikt door het pakketbeheersysteem naar een van de vele HTTP-spiegelservers
van Debian te verwijzen. Een uitgebreide lijst met spiegelservers is
beschikbaar op:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Oplossingen voor diverse problemen</h2>

<p>Met deze update van stable, de stabiele distributie, worden een paar
belangrijke correcties aangebracht aan de volgende pakketten:</p>

<table border=0>
<tr><th>Pakket</th>               <th>Reden</th></tr>
<correction avahi "Weergave van URL's die '&amp;' bevatten in avahi-discover repareren; tijdslimietopschoning niet uitschakelen bij een toezichtsopschoning; reparatie voor NULL pointer crashes bij het proberen opzoeken van slecht opgemaakte computernamen [CVE-2021-3502]">
<correction base-files "Updaten van /etc/debian_version voor de tussenrelease 11.5">
<correction cargo-mozilla "Nieuw broncodepakket om het bouwen van recentere versies van firefox-esr en thunderbird te ondersteunen">
<correction clamav "Nieuwe bovenstroomse stabiele release">
<correction commons-daemon "JVM-detectie herstellen">
<correction curl "Cookies met <q>controlebytes</q> afwijzen [CVE-2022-35252]">
<correction dbus-broker "Oplossing voor beweringsfouten bij het ontkoppelen van peergroepen; geheugenlek repareren; oplossen van probleem van null pointer dereference [CVE-2022-31213]">
<correction debian-installer "Hercompilatie tegen proposed-updates; Linux kernel ABI verhogen naar 5.10.0-18">
<correction debian-installer-netboot-images "Hercompilatie tegen proposed-updates; Linux kernel ABI verhogen naar 5.10.0-18">
<correction debian-security-support "Bijwerken van de beveiligingstoestand van verschillende pakketten">
<correction debootstrap "Ervoor zorgen dat chroots met niet-samengevoegde usr-mappen aangemaakt kunnen blijven worden voor oudere releases en buildd-chroots">
<correction dlt-daemon "Probleem met dubbele vrijgave oplossen [CVE-2022-31291]">
<correction dnsproxy "Standaard op localhost luisteren, in plaats van op het mogelijk onbeschikbare 192.168.168.1">
<correction dovecot "Mogelijke beveiligingsproblemen oplossen wanneer er twee passdb configuratie-items bestaan met dezelfde driver- en args-instellingen [CVE-2022-30550]">
<correction dpkg "Reparatie van de verwerking van conf-bestandsverwijdering bij opwaardering, geheugenlek in de verwerking van de opdracht removal-on-upgrade; Dpkg::Shlibs::Objdump: oplossing om apply_relocations te laten werken met versiegebonden symbolen; toevoegen van ondersteuning voor CPU ARCv2; verschillende updates en reparaties aan dpkg-fsys-usrunmess">
<correction fig2dev "Oplossen van probleem met dubbele vrijgave [CVE-2021-37529] en probleem van denial of service [CVE-2021-37530]; verkeerde plaatsing van ingesloten eps-afbeeldingen stoppen">
<correction foxtrotgps "Oplossen van een crash door ervoor te zorgen dat threads altijd niet-gerefereerd zijn">
<correction gif2apng "Oplossen van problemen met heap bufferoverloop [CVE-2021-45909 CVE-2021-45910 CVE-2021-45911]">
<correction glibc "Reparatie voor een off-by-one buffer overflow/underflow in getcwd() [CVE-2021-3999]; repareren van verschillende overlopen in functies voor brede tekens; toevoeging van enkele voor EVEX geoptimaliseerde stringfuncties voor het oplossen van een prestatieprobleem (tot wel 40%) met Skylake-X processors; grantpt bruikbaar maken na multi-threaded fork; ervoor zorgen dat libio vtable-bescherming is ingeschakeld">
<correction golang-github-pkg-term "Reparatie voor compilatie op recentere Linux kernels">
<correction gri "Gebruik maken van <q>ps2pdf</q> in plaats van <q>convert</q> om PS te converteren naar PDF">
<correction grub-efi-amd64-signed "Nieuwe bovenstroomse release">
<correction grub-efi-arm64-signed "Nieuwe bovenstroomse release">
<correction grub-efi-ia32-signed "Nieuwe bovenstroomse release">
<correction grub2 "Nieuwe bovenstroomse release">
<correction http-parser "Voor nieuwe Transfer-Encoding F_CHUNKED uitschakelen , waardoor mogelijk probleem met smokkel van HTTP-verzoeken wordt opgelost [CVE-2020-8287]">
<correction ifenslave "Oplossing voor de configuratie van verbonden interfaces">
<correction inetutils "Reparatie voor een probleem van bufferoverloop [CVE-2019-0053], een probleem van uitputting van de stack, de behandeling van FTP PASV-antwoorden [CVE-2021-40491] en een probleem van denial of service [CVE-2022-39028]">
<correction knot "Oplossing voor terugval van IXFR naar AXFR met dnsmasq">
<correction krb5 "SHA256 gebruiken als frommel voor Pkinit CMS">
<correction libayatana-appindicator "Compatibiliteit bieden voor software die afhankelijk is van libappindicator">
<correction libdatetime-timezone-perl "Bijwerken van opgenomen gegevens">
<correction libhttp-daemon-perl "De verwerking verbeteren van de kopregel Content-Length [CVE-2022-31081]">
<correction libreoffice "Ondersteuning bieden voor de EUR in de regionale configuratie .hr; omrekeningskoers HRK&lt;-&gt;EUR toevoegen in Calc en in de Euro-wizard; beveiligingsoplossingen [CVE-2021-25636 CVE-2022-26305 CVE-2022-26306 CVE-2022-26307]; oplossing voor vastlopen bij het benaderen van het adresboek van Evolution">
<correction linux "Nieuwe bovenstroomse stabiele release">
<correction linux-signed-amd64 "Nieuwe bovenstroomse stabiele release">
<correction linux-signed-arm64 "Nieuwe bovenstroomse stabiele release">
<correction linux-signed-i386 "Nieuwe bovenstroomse stabiele release">
<correction llvm-toolchain-13 "Nieuw broncodepakket om het bouwen van recentere versies van firefox-esr en thunderbird te ondersteunen">
<correction lwip "Problemen van bufferoverloop oplossen [CVE-2020-22283 CVE-2020-22284]">
<correction mokutil "Nieuwe bovenstroomse versie om SBAT-beheer mogelijk te maken">
<correction node-log4js "Niet standaard wereldwijd leesbare bestanden aanmaken [CVE-2022-21704]">
<correction node-moment "Probleem met denial of service op basis van reguliere expressies oplossen [CVE-2022-31129]">
<correction nvidia-graphics-drivers "Nieuwe bovenstroomse release; beveiligingsoplossingen [CVE-2022-31607 CVE-2022-31608 CVE-2022-31615]">
<correction nvidia-graphics-drivers-legacy-390xx "Nieuwe bovenstroomse release; beveiligingsoplossingen [CVE-2022-31607 CVE-2022-31608 CVE-2022-31615]">
<correction nvidia-graphics-drivers-tesla-450 "Nieuwe bovenstroomse release; beveiligingsoplossingen [CVE-2022-31607 CVE-2022-31608 CVE-2022-31615]">
<correction nvidia-graphics-drivers-tesla-470 "Nieuwe bovenstroomse release; beveiligingsoplossingen [CVE-2022-31607 CVE-2022-31608 CVE-2022-31615]">
<correction nvidia-settings "Nieuwe bovenstroomse release; herstelling voor kruiscompilatie">
<correction nvidia-settings-tesla-470 "Nieuwe bovenstroomse release; herstelling voor kruiscompilatie">
<correction pcre2 "Oplossen van problemen van lezen buiten de grenzen [CVE-2022-1586 CVE-2022-1587]">
<correction postgresql-13 "Laat extensiescripts geen objecten vervangen die niet reeds tot de extensie behoren [CVE-2022-2625]">
<correction publicsuffix "Bijwerken van opgenomen gegevens">
<correction rocksdb "Ongeldige instructie op arm64 verhelpen">
<correction sbuild "Buildd::Mail: MIME-gecodeerde kopregel Subject: ondersteunen en ook de kopregel Content-Type: kopiëren bij het doorsturen van e-mail">
<correction systemd "Gebundelde kopie van linux/if_arp.h laten vallen als oplossing voor compilatiefouten met nieuwere kernelheaders; detectie van ARM64 Hyper-V gasten ondersteunen; OpenStack-dienst op arm detecteren als KVM">
<correction twitter-bootstrap4 "De CSS-omzettingsbestanden daadwerkelijk installeren">
<correction tzdata "Tijdzonegegevens voor Iran en Chili bijwerken">
<correction xtables-addons "Zowel oude als nieuwe versies van security_skb_classify_flow() ondersteunen">
</table>


<h2>Beveiligingsupdates</h2>


<p>Met deze revisie worden de volgende beveiligingsupdates toegevoegd aan de
stabiele release. Het beveiligingsteam heeft voor elk van deze updates
al een advies uitgebracht:</p>

<table border=0>
<tr><th>Advies-ID</th>  <th>Pakket</th></tr>
<dsa 2022 5175 thunderbird>
<dsa 2022 5176 blender>
<dsa 2022 5177 ldap-account-manager>
<dsa 2022 5178 intel-microcode>
<dsa 2022 5179 php7.4>
<dsa 2022 5180 chromium>
<dsa 2022 5181 request-tracker4>
<dsa 2022 5182 webkit2gtk>
<dsa 2022 5183 wpewebkit>
<dsa 2022 5184 xen>
<dsa 2022 5185 mat2>
<dsa 2022 5187 chromium>
<dsa 2022 5188 openjdk-11>
<dsa 2022 5189 gsasl>
<dsa 2022 5190 spip>
<dsa 2022 5191 linux-signed-amd64>
<dsa 2022 5191 linux-signed-arm64>
<dsa 2022 5191 linux-signed-i386>
<dsa 2022 5191 linux>
<dsa 2022 5192 openjdk-17>
<dsa 2022 5193 firefox-esr>
<dsa 2022 5194 booth>
<dsa 2022 5195 thunderbird>
<dsa 2022 5196 libpgjava>
<dsa 2022 5197 curl>
<dsa 2022 5198 jetty9>
<dsa 2022 5199 xorg-server>
<dsa 2022 5200 libtirpc>
<dsa 2022 5201 chromium>
<dsa 2022 5202 unzip>
<dsa 2022 5203 gnutls28>
<dsa 2022 5204 gst-plugins-good1.0>
<dsa 2022 5205 ldb>
<dsa 2022 5205 samba>
<dsa 2022 5206 trafficserver>
<dsa 2022 5207 linux-signed-amd64>
<dsa 2022 5207 linux-signed-arm64>
<dsa 2022 5207 linux-signed-i386>
<dsa 2022 5207 linux>
<dsa 2022 5208 epiphany-browser>
<dsa 2022 5209 net-snmp>
<dsa 2022 5210 webkit2gtk>
<dsa 2022 5211 wpewebkit>
<dsa 2022 5213 schroot>
<dsa 2022 5214 kicad>
<dsa 2022 5215 open-vm-tools>
<dsa 2022 5216 libxslt>
<dsa 2022 5217 firefox-esr>
<dsa 2022 5218 zlib>
<dsa 2022 5219 webkit2gtk>
<dsa 2022 5220 wpewebkit>
<dsa 2022 5221 thunderbird>
<dsa 2022 5222 dpdk>
</table>


<h2>Verwijderde pakketten</h2>

<p>De volgende pakketten werden verwijderd wegens omstandigheden waarover wij geen controle hebben:</p>

<table border=0>
<tr><th>Pakket</th>               <th>Reden</th></tr>
<correction evenement "Onbeheerd; alleen nodig voor reeds verwijderde movim">
<correction php-cocur-slugify "Onbeheerd; alleen nodig voor reeds verwijderde movim">
<correction php-defuse-php-encryption "Onbeheerd; alleen nodig voor reeds verwijderde movim">
<correction php-dflydev-fig-cookies "Onbeheerd; alleen nodig voor reeds verwijderde movim">
<correction php-embed "Onbeheerd; alleen nodig voor reeds verwijderde movim">
<correction php-fabiang-sasl "Onbeheerd; alleen nodig voor reeds verwijderde movim">
<correction php-markdown "Onbeheerd; alleen nodig voor reeds verwijderde movim">
<correction php-raintpl "Onbeheerd; alleen nodig voor reeds verwijderde movim">
<correction php-react-child-process "Onbeheerd; alleen nodig voor reeds verwijderde movim">
<correction php-react-http "Onbeheerd; alleen nodig voor reeds verwijderde movim">
<correction php-respect-validation "Onbeheerd; alleen nodig voor reeds verwijderde movim">
<correction php-robmorgan-phinx "Onbeheerd; alleen nodig voor reeds verwijderde movim">
<correction ratchet-pawl "Onbeheerd; alleen nodig voor reeds verwijderde movim">
<correction ratchet-rfc6455 "Onbeheerd; alleen nodig voor reeds verwijderde movim">
<correction ratchetphp "Onbeheerd; alleen nodig voor reeds verwijderde movim">
<correction reactphp-cache "Onbeheerd; alleen nodig voor reeds verwijderde movim">
<correction reactphp-dns "Onbeheerd; alleen nodig voor reeds verwijderde movim">
<correction reactphp-event-loop "Onbeheerd; alleen nodig voor reeds verwijderde movim">
<correction reactphp-promise-stream "Onbeheerd; alleen nodig voor reeds verwijderde movim">
<correction reactphp-promise-timer "Onbeheerd; alleen nodig voor reeds verwijderde movim">
<correction reactphp-socket "Onbeheerd; alleen nodig voor reeds verwijderde movim">
<correction reactphp-stream "Onbeheerd; alleen nodig voor reeds verwijderde movim">

</table>

<h2>Het Debian-installatiesysteem</h2>
<p>Het installatiesysteem werd bijgewerkt om de reparaties die met deze tussenrelease in stable, de stabiele release, opgenomen werden, toe te voegen.</p>

<h2>URL's</h2>

<p>De volledige lijsten met pakketten die met deze revisie gewijzigd werden:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>De huidige stabiele distributie:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Voorgestelde updates voor de stabiele distributie:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>informatie over de stabiele distributie (notities bij de release, errata, enz.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Beveiligingsaankondigingen en -informatie:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Over Debian</h2>

<p>Het Debian-project is een vereniging van ontwikkelaars van vrije software
die vrijwillig tijd en moeite steken in het produceren van het volledig vrije
besturingssysteem Debian.</p>

<h2>Contactinformatie</h2>

<p>Ga voor verdere informatie naar de webpagina's van Debian op
<a href="$(HOME)/">https://www.debian.org/</a>, stuur een e-mail naar
&lt;press@debian.org&gt;, of neem contact met het release-team voor de stabiele
release op &lt;debian-release@lists.debian.org&gt;.</p>


