#use wml::debian::template title="Debian &ldquo;trixie&rdquo; 发行信息"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/trixie/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="6bafc9758f46d9e4fe5b48e5d85484d9f3dc8e3e"

<if-stable-release release="trixie">

<p>Debian <current_release_trixie> 已\
于 <a href="$(HOME)/News/<current_release_newsurl_trixie/>"><current_release_date_trixie></a> 发布。
<ifneq "13.0" "<current_release>"
  "Debian 13.0 最初发布于 <:=spokendate('XXXXXXXX'):>。"
/>
此次发行包含了许多重要的\
变更，在\
我们的<a href="$(HOME)/News/XXXX/XXXXXXXX">新闻稿</a>与\
<a href="releasenotes">发行说明</a>有详细的介绍。</p>

#<p><strong>Debian 13 has been superseded by
#<a href="../forky/">Debian 14 (<q>forky</q>)</a>.
#Security updates have been discontinued as of <:=spokendate('xxxx-xx-xx'):>.
#</strong></p>

### This paragraph is orientative, please review before publishing!
#<p><strong>However, trixie benefits from Long Term Support (LTS) until
#the end of xxxxx 20xx. The LTS is limited to i386, amd64, armel, armhf and arm64.
#All other architectures are no longer supported in trixie.
#For more information, please refer to the <a
#href="https://wiki.debian.org/LTS">LTS section of the Debian Wiki</a>.
#</strong></p>

<p>要取得与安装 Debian，请见\
<a href="debian-installer/">安装信息</a>页面与\
<a href="installmanual">安装手册</a>。要从较旧的 Debian 发行版\
升级，请见\
<a href="releasenotes">发行说明</a>的操作指引。</p>

### Activate the following when LTS period starts.
#<p>Architectures supported during Long Term Support:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>以下是 trixie 最初发布时支持的计算机架构：</p>

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>事与愿违，发行版即使被公开宣布是\
<em>稳定的（stable）</em>，仍可能会存在一些问题。我们已制作了\
<a href="errata">重要已知问题列表</a>，您也可以随时\
<a href="../reportingbugs">回报其他问题</a>给我们。</p>

<p>最后值得一提的是，我们有个<a href="credits">鸣谢</a>\
列表，列出为此次发行版做出贡献的人。</p>
</if-stable-release>

<if-stable-release release="bookworm">

<p>在 <a href="../bookworm/">bookworm</a> 之后的下一个 \
Debian 主版本的代号为 <q>trixie</q>。</p>

<p>此版本以 bookworm 的副本为起点，目前处于被称作\
<q><a href="$(DOC)/manuals/debian-faq/ftparchives#testing">测试</a></q>的\
阶段。\
这意味着出现的问题不应该像不稳定版（unstable）或是实验版（experimental）\
那样严重，因为只有\
在经过一段时间后，且没有任何对发布关键的缺陷（release-critical bug）被\
报告时，软件包才会被允许进入这个版本。</p>

<p>请注意，<q>测试</q>发行版的安全更新还\
<strong>未</strong>由安全团队管理。因此，<q>测试</q>版\
<strong>不能</strong>及时获取安全更新。\
# For more information please see the
# <a href="https://lists.debian.org/debian-testing-security-announce/2008/12/msg00019.html">announcement</a>
# of the Testing Security Team.
如果您需要安全支持，鼓励您暂时切换您的 \
sources.list 条目，\
从 testing 到 bookworm。另请参见有关\
<q>测试</q>发行版的\
<a href="$(HOME)/security/faq#testing">安全团队 FAQ</a>。</p>

<p>可能有一份<a href="releasenotes">发行说明草案</a>。\
另请<a href="https://bugs.debian.org/release-notes">查看\
建议向发行说明添加的内容</a>。</p>

<p>有关<q>testing</q>的安装映像及如何安装的文档，\
请参阅 <a href="$(HOME)/devel/debian-installer/">Debian 安装程序页面</a>。</p>

<p>要了解更多<q>测试</q>发行版的工作方式，请查看\
<a href="$(HOME)/devel/testing">开发人员相关信息</a>。</p>

<p>人们经常询问是否有一个单独的发布<q>进度表</q>。\
不幸的是没有，但我们可以将你引到几个地方，其中描述了\
发布新版本之前必须完成的事项：</p>

<ul>
  <li><a href="https://release.debian.org/">通用发布状态页面</a></li>
  <li><a href="https://bugs.debian.org/release-critical/">对发布关键的缺陷</a></li>
  <li><a href="https://udd.debian.org/bugs.cgi?base=only&amp;rc=1">基本系统缺陷</a></li>
  <li><a href="https://udd.debian.org/bugs.cgi?standard=only&amp;rc=1">标准和任务软件包中的缺陷</a></li>
</ul>

<p>此外，发布管理者也会发布一般状态报告\
到 <a href="https://lists.debian.org/debian-devel-announce/">\
debian-devel-announce 邮件列表</a>。</p>

</if-stable-release>

<if-stable-release release="bullseye">

<p>The code name for the next major Debian release after <a
href="../bookworm/">bookworm</a> is <q>trixie</q>. Currently,
<q>bookworm</q> is not released yet. So <q>trixie</q> is still
far away.</p>

</if-stable-release>
