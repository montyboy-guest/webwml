#use wml::debian::template title="安全信息" GEN_TIME="yes" MAINPAGE="true"
#use wml::debian::toc
#use wml::debian::recent_list_security
#use wml::debian::translation-check translation="e49cfbc5f5d8df40e0852a5da747d32544d67b06" maintainer="Boyuan Yang"

# Translator: dimension <g9113@cs.nccu.edu.tw>, Tue Dec 10 13:32:52 CST 2002
# Checked by chihchun, Wed, 18 Dec 2002 10:26:23 +0800
# $Id$

#include "$(ENGLISHDIR)/releases/info"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#keeping-secure">保持您 Debian 系统的安全性</a></li>
<li><a href="#DSAS">最新警报</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian 對於系統安全採取十分嚴謹的態度。一旦有安全漏洞被發現，我們會確保在合理的時間內加以修正。</p>
</aside>

<p>
經驗告訴我們，<q>越隱蔽越安全</q>這觀念是錯誤的。就安全問題而言，把
[CN:信息:][HKTW:資訊:]公開可更快地找到更好的解決方法。有見及此，本網頁
列出已知的、並可能會對 Debian 操作系统構成威脅的安全漏洞。
</p>

<p>
許多安全警報是和其他自由軟體廠商合作，在該弱點被公開的同一天內發布。
</p>
  
# "reasonable timeframe" might be too vague, but we don't have 
# accurate statistics. For older (out of date) information and data
# please read:
# https://www.debian.org/News/2004/20040406  [ Year 2004 data ]
# and (older)
# https://people.debian.org/~jfs/debconf3/security/ [ Year 2003 data ]
# https://lists.debian.org/debian-security/2001/12/msg00257.html [ Year 2001]
# If anyone wants to do up-to-date analysis please contact me (jfs)
# and I will provide scripts, data and database schemas.


<p>
Debian 也參與了安全防護標準化的工作：
</p>

<ul>
  <li><a href="#DSAS">Debian 安全警報</a>是 <a href="cve-compatibility">CVE-兼容</a>的（参见<a href="crossreferences">交互參照</a>）。</li>
  <li>Debian 是 <a href="https://oval.cisecurity.org/">Open Vulnerability Assessment Language</a> 計劃的一員。</li>
</ul>

<h2><a id="keeping-secure">保持您 Debian 系统的安全性</a></h2>


<p>
想收到最新的 Debian 安全警報的話，請訂閱 <a href="https://lists.debian.org/debian-security-announce/">debian-security-announce</a> 郵件列表。
</p>

<p>
此外，您还可以使用 <a href="https://packages.debian.org/stable/admin/apt">APT</a> 来方便地获取最新的安全更新。要使您的 Debian 操作系统获得最新的安全更新，请把下面这行加入到您的 <code>/etc/apt/sources.list</code> 文件中：
</p>

<pre>
deb http://security.debian.org/debian-security <current_release_security_name> main contrib non-free non-free-firmware
</pre>

<p>
保存修改后，请执行以下两个命令以下载并安装安全更新：
</p>

<pre>
apt-get update &amp;&amp; apt-get upgrade
</pre> 

<p>
安全仓库使用通常的 <a href="https://ftp-master.debian.org/keys.html">Debian 仓库密钥</a>进行签名。
</p>

<p>
想得到关于 Debian 安全问题的更多[CN:信息:][HKTW:資訊:]，請阅读我们的 FAQ 和文档：
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="faq">安全 FAQ</a></button> <button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="../doc/user-manuals#securing">Debian 安全手册</a></button></p>


<aside class="light">
  <span class="fa fa-rss fa-5x"></span>
</aside>

<h2><a id="DSAS">最新警报</a></h2>

<p>以下页面是发布在 <a href="https://lists.debian.org/debian-security-announce/">\
debian-security-announce</a> 邮件列表的安全警报的简要存档。<br>
<a href="dsa.html#DSAS">新版格式列表</a>

<p>
<:= get_recent_security_list( '1m', '6', '.', '$(ENGLISHDIR)/security' ) :>
</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Debian Security Advisories (titles only)" href="dsa">
<link rel="alternate" type="application/rss+xml"
 title="Debian Security Advisories (summaries)" href="dsa-long">
:#rss#}

<p>
Debian 的最新安全警报同時也提供 <a href="dsa">RDF</a> 的格式。我們也提供<a href="dsa-long">另一個更长的版本</a>，它包含了相對應的警报的第一段，这样您就可以很轻松地看出該警报是關於什麼的。
</p>

#include "$(ENGLISHDIR)/security/index.include"
<p>您也可以查看更旧的安全警报：
<:= get_past_sec_list(); :>

<p>Debian 各發行版不是全部都存在有所有安全問題。
<a href="https://security-tracker.debian.org/">Debian 安全追蹤網</a>收集了
所有 Debian 套件有關的弱點資訊，可以透過 CVE 名稱或套件名稱搜尋。</p>
