#use wml::debian::template title="[CN:獲取:][HKTW:取得:] Debian"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="b0ca47afc4da28aed730cba13527a353725daef8" maintainer="Anthony Wong"

# $Id$
# Translator: Allen Huang <allen.huang@seed.net.tw>, Fri Nov 15 12:00:15 CST 2002
# Checked by cctsai, 2002/12/26

<p>本页面提供了几种安装 Debian 稳定版的方式。


<ul>
<li> 可下载安装映像的<a href="../CD/http-ftp/#mirrors">[CN:鏡像站點:][HKTW:映射站台:]</a>
<li> 包含详细安装步骤的<a href="../releases/stable/installmanual">安装手册</a>
<li> <a href="../releases/stable/releasenotes">发行说明</a>
<li> <a href="../releases/">其他发行版本</a>，例如测试版（testing）或不稳定版（unstable）
</ul>
</p>

<div class="line">
  <div class="item col50">
    <h2><a href="netinst">下載一個安裝映像</a></h2>
    <ul>
      <li>一個<a href="netinst"><strong>小型安裝映像</strong></a>：\
	    可以被很快地下載到您的計算機，並須要把它\
	    複製到可[CN:移动:][HKTW:移除:]的媒介，\
	    如光碟、[CN:U&nbsp;盤:][HK:USB&nbsp;手指:][TW:隨身碟:]等。\
	    安裝過程中，您的計算機需要[CNHK:互聯網:][TW:網際網路:]連線。
	<ul class="quicklist downlist">
	  <li><a title="下載 64 位 Intel 和 AMD PC 適用的安裝程式"
	         href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">64 位 PC 网络安装 iso</a></li>
	  <li><a title="下載一般 32 位 Intel 和 AMD PC 適用的安裝程式"
	         href="<stable-images-url/>/i386/iso-cd/debian-<current-tiny-cd-release-filename/>-i386-netinst.iso">32 位 PC 网络安装 iso</a></li>
	</ul>
      </li>
      <li>較龐大的<a href="../CD/"><strong>完整安裝映像</strong></a>：\
	包含了更多的軟件包，以便在無法連上[CNHK:互聯網:][TW:網際網路:]的計算機上進行安裝。
	<ul class="quicklist downlist">
	  <li><a title="下載 64 位 Intel 和 AMD PC 適用的 DVD torrent 種子"
	         href="<stable-images-url/>/amd64/bt-dvd/">64 位 PC torrent 種子 (DVD)</a></li>
	  <li><a title="下載一般 32 位 Intel 和 AMD PC 適用的 DVD torrent 種子"
	         href="<stable-images-url/>/i386/bt-dvd/">32 位 PC torrent 種子 (DVD)</a></li>
	  <li><a title="下載 64 位 Intel 和 AMD PC 適用的 CD torrent 種子"
	         href="<stable-images-url/>/amd64/bt-cd/">64 位 PC torrent 種子 (CD)</a></li>
	  <li><a title="下載一般 32 位 Intel 和 AMD PC 適用的 CD torrent 種子"
	         href="<stable-images-url/>/i386/bt-cd/">32 位 PC torrent 種子 (CD)</a></li>
	</ul>
      </li>
    </ul>
  </div>
  <div class="item col50 lastcol">
    <h2><a href="https://cloud.debian.org/images/cloud/">使用 Debian 云映像</a></h2>
    <p>由云团队构建的官方的<a href="https://cloud.debian.org/images/cloud/"><strong>云映像</strong></a>，可被用于：</p>
    <ul>
      <li>您的 OpenStack 提供商，包括 qcow2 和 raw 格式。
      <ul class="quicklist downlist">
          <li>64 位 AMD/Intel (<a title="用于 64 位 AMD/Intel 的 OpenStack 映像，qcow2" href="https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-generic-amd64.qcow2">qcow2</a>, <a title="用于 64 位 AMD/Intel 的 OpenStack 映像，raw" href="https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-generic-amd64.raw">raw</a>)</li>
       <li>64 位 ARM (<a title="用于 64 位 ARM 的 OpenStack 映像，qcow2" href="https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-generic-arm64.qcow2">qcow2</a>, <a title="用于 64 位 ARM 的 OpenStack 映像，raw" href="https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-generic-arm64.raw">raw</a>)</li>
          <li>64 位小端序 PowerPC (<a title="用于 64 位小端序 PowerPC 的 OpenStack 映像，qcow2" href="https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-generic-ppc64el.qcow2">qcow2</a>, <a title="用于 64 位小端序 PowerPC 的 OpenStack 映像，raw" href="https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-generic-ppc64el.raw">raw</a>)</li>
      </ul>
      </li>
      <li>本地 QEMU 虚拟机，包括 qcow2 和 raw 格式。
      <ul class="quicklist downlist">
	   <li>64 位 AMD/Intel (<a title="用于 64 位 AMD/Intel 的 QEMU 映像，qcow2" href="https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-nocloud-amd64.qcow2">qcow2</a>, <a title="用于 64 位 AMD/Intel 的 QEMU 映像，raw" href="https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-nocloud-amd64.raw">raw</a>)</li>
       <li>64 位 ARM (<a title="用于 64 位 ARM 的 QEMU 映像，qcow2" href="https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-nocloud-arm64.qcow2">qcow2</a>, <a title="用于 64 位 ARM 的 QEMU 映像，raw" href="https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-nocloud-arm64.raw">raw</a>)</li>
	   <li>64 位小端序 PowerPC (<a title="用于 64 位小端序 PowerPC 的 QEMU 映像，qcow2" href="https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-nocloud-ppc64el.qcow2">qcow2</a>, <a title="用于 64 位小端序 PowerPC 的 QEMU 映像，raw" href="https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-nocloud-ppc64el.raw">raw</a>)</li>
      </ul>
      </li>
      <li>Amazon EC2，作为机器映像或通过 AWS 市场提供。
          <ul class="quicklist downlist">
           <li><a title="Amazon 机器映像" href="https://wiki.debian.org/Cloud/AmazonEC2Image/">Amazon 机器映像</a></li>
           <li><a title="AWS 市场" href="https://aws.amazon.com/marketplace/seller-profile?id=4d4d4e5f-c474-49f2-8b18-94de9d43e2c0">AWS 市场</a></li>
          </ul>
      </li>
      <li>Microsoft Azure，通过 Azure 市场提供。
          <ul class="quicklist downlist">
           <li><a title="Azure 市场中的 Debian 12" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-12?tab=PlansAndPrice">Debian 12 ("Bookworm")</a></li>
           <li><a title="Azure 市场中的 Debian 11" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-11?tab=PlansAndPrice">Debian 11 ("Bullseye")</a></li>
          </ul>
      </li>
    </ul>
  </div>
</div>
<div class="line">
  <div class="item col50">
   <h2><a href="../CD/vendors/">從銷售 Debian 安装介质的[CN:經銷商:][HKTW:供應商:]購買 CD、DVD 或 [CN:U&nbsp;盤:][HK:USB&nbsp;手指:][TW:USB&nbsp;隨身碟:]。</a></h2>
   <p>
      大多數[CN:經銷商:][HKTW:供應商:]的售價低於 5 美元（您可以查看他们的网站了解他們是否提供跨國[CN:銷售:][HKTW:販賣:]）。
   </p>

   <p>以下是使用[CN:光盤:][HKTW:光碟:]的優勢：</p>
   <ul>
     <li>您可以在沒有[CNHK:網絡:][TW:網路:]的機器上進行安裝。</li>
     <li>您不必自己下載所有軟件包就可以进行安装。</li>
   </ul>

   <h2><a href="pre-installed">購買一台預裝 Debian 的計算機</a></h2>
   <p>這麼做的好處是：</p>
   <ul>
    <li>您不必親自安裝 Debian。</li>
    <li>安裝過程已經針對硬件作了預先配置。</li>
    <li>[CN:經銷商:][HKTW:供應商:]將為您提供技術支持。</li>
   </ul>
  </div>
  <div class="item col50 lastcol">
    <h2><a href="../CD/live/">安裝前先體驗 Debian Live<q>現場版</q></a></h2>
    <p>
      現在您可以不用安裝任何[CN:文件:][TWHK:文檔:]到計算機上，\
      就能通过从 CD、DVD 或 [CN: U 盤:][HK: USB 手指:][TW:USB 隨身碟:]啟動
      一个<q>現場版</q>live 系統来试用 Debian。\
      您还可以运行内置的 <a href="https://calamares.io">Calamares 安装器</a>\
      。仅提供 64 位 PC 版本。\
      详情请阅读<a href="../CD/live#choose_live">关于此安装方式的更多信息</a>。
    </p>
    <ul class="quicklist downlist">
      <li><a title="下载用于 64 位 Intel 和 AMD PC 的 Gnome live ISO"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-gnome.iso">Live Gnome</a></li>
      <li><a title="下载用于 64 位 Intel 和 AMD PC 的 Xfce live ISO"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-xfce.iso">Live Xfce</a></li>
      <li><a title="下载用于 64 位 Intel 和 AMD PC 的 KDE live ISO"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-kde.iso">Live KDE</a></li>
      <li><a title="下载用于 64 位 Intel 和 AMD PC 的其他 live ISO"
            href="<live-images-url/>/amd64/iso-hybrid/">其他 live ISO</a></li>
      <li><a title="下载用于 64 位 Intel 和 AMD PC 的 live torrent 种子"
          href="<live-images-url/>/amd64/bt-hybrid/">live torrent 种子</a></li>
</ul>
  </div>

</div>
