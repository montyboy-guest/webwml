#use wml::debian::translation-check translation="8ca2f9ee4f77953d340bf460e23918f2a3b96785" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une divulgation de descripteur de fichier
dans le bus de message D-Bus.</p>

<p>Un attaquant local non privilégié pourrait utiliser cela pour attaquer le
démon du système DBus, conduisant à un déni de service pour tous les
utilisateurs de la machine.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12049">CVE-2020-12049</a>

<p>Divulgation de descripteur de fichier dans _dbus_read_socket_with_unix_fds.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.8.22-0+deb8u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets dbus.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2235.data"
# $Id: $
