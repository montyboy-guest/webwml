#use wml::debian::translation-check translation="51b1ddef24f90f4324cd78e53a3dd62f02ec4ae4" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans vim, un éditeur
vi moderne. Des dépassements de tampon, des lectures hors limites et des
utilisations de mémoire après libération pourraient conduire à un déni de
service (plantage d'application) ou à un autre impact non précisé.</p>

<p>Pour Debian 10 « Buster », ces problèmes ont été corrigés dans
la version 2:8.1.0875-5+deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets vim.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de vim,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/vim">\
https://security-tracker.debian.org/tracker/vim</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3182.data"
# $Id: $
