#use wml::debian::translation-check translation="74294814f2a93794742137b24e84dc19fd68e8cb" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans heimdal, une
implémentation du protocole d’authentification Kerberos 5, qui peut aboutir à
un déni de service, une divulgation d'informations ou une exécution de code à
distance.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14870">CVE-2019-14870</a>

<p>Isaac Boukris a signalé que le KDC d’Heimdal avant la version 7.7.1
n’appliquait pas les attributs d’utilisateur delegation_not_allowed
(c’est-à-dire not-delegated) pour S4U2Self. À la place le drapeau forwardable est
défini même si le client usurpé a le drapeau not-delegated établi.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3671">CVE-2021-3671</a>

<p>Joseph Sutton a découvert que le KDC d’Heimdal avant la version 7.7.1 ne
vérifie pas les sname manquants dans TGS-REQ (Ticket serveur Granting-Request)
avant le déréférencement. Un utilisateur authentifié pourrait utiliser ce défaut
pour planter le KDC.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44758">CVE-2021-44758</a>

<p>Il a été découvert qu’Heimdal est prédisposé à un déréférencement de pointeur
NULL dans les accepteurs quand le jeton SPNEGO initial n'a pas de mécanisme.
Cela peut aboutir à un déni de service pour une application de serveur qui
utilise SPNEGO (Simple et Protected négociation GSSAPI Mechanism).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3437">CVE-2022-3437</a>

<p>Evgeny Legerov a signalé que les routines de déchiffrement DES et Triple-DES
dans la bibliothèque GSSAPI avant la version 7.7.1 d’Heimdal étaient prédisposées
à un dépassement de tampon dans la mémoire allouée par malloc() quand un petit
paquet malveillant était présenté. De plus, la routine de déchiffrement
Triple-DES et RC4 (arcfour) était prédisposée à des fuites de durée non
constantes, qui pouvaient éventuellement amener à une fuite des données de clé
secrète lors de l’utilisation de ces chiffrements.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41916">CVE-2022-41916</a>

<p>Il a été découvert que la bibliothèque de validation de certificat PKI
d’Heimdal avant la version 7.7.1 peut sous certaines circonstances réaliser un
accès en mémoire hors limites lors d’une normalisation en Unicode, pouvant
aboutir à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42898">CVE-2022-42898</a>

<p>Greg Hudson a découvert un dépassement de multiplication d’entier dans la
routine d’analyse PAC (Privilege Attribute Certificate), qui peut aboutir à un
déni de service pour les KDC d’Heimdal et éventuellement de ses serveurs
(par exemple, à l’aide de GSS-API) sur les systèmes 32 bits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-44640">CVE-2022-44640</a>

<p>Douglas Bagnall et les responsables d’Heimdal indépendamment ont découvert
qu’ASN.1 du compilateur d’Heimdal avant la version 7.7.1 générait du code qui
permettait à des encodages DER contrefaits pour l'occasion de CHOICE d’invoquer
la mauvaise fonction free() sur la structure décodée lors d’une erreur de
décodage. Cela pouvait aboutir à une exécution de code à distance dans le KDC
d’Heimdal et éventuellement dans le client Kerberos, la bibliothèque X.509 et
aussi d’autres composants.</p></li>

</ul>

<p>Pour Debian 10 « Buster », ces problèmes ont été corrigés dans
la version 7.5.0+dfsg-3+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets heimdal.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de heimdal,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/heimdal">\
https://security-tracker.debian.org/tracker/heimdal</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3206.data"
# $Id: $
