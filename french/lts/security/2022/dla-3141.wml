#use wml::debian::translation-check translation="aa717530161e520f07a8cb93216b911c546bcad3" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans Wordpress,
un cadriciel populaire de gestion de contenus. Des attaques SSRF (Server
Side Request Forgery) et de script intersite (XSS) peuvent faciliter le
contournement des contrôles d'accès ou l'injection de scripts côté client.</p>

<p>Pour Debian 10 Buster, ce problème a été corrigé dans la version
5.0.17+dfsg1-0+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets wordpress.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de wordpress, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/wordpress">\
https://security-tracker.debian.org/tracker/wordpress</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3141.data"
# $Id: $
