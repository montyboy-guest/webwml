#use wml::debian::translation-check translation="55d1ac616e9ec6fe42ad1680e45c2ce133b85547" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de corruption de mémoire ont été identifiés dans
libtiff et les outils associés.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9273">CVE-2016-9273</a>

<p>Dépassement de tas dans cpStrips().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9297">CVE-2016-9297</a>

<p>Lecture en dehors du tampon dans _TIFFPrintField().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9532">CVE-2016-9532</a>

<p>Dépassement de tas à l'aide de writeBufferToSeparateStrips().</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 4.0.2-6+deb7u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tiff.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-716.data"
# $Id: $
