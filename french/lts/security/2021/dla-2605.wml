#use wml::debian::translation-check translation="d836c9689095f8e4a6da4f99789d77f1f26e9ec6" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un problème d’exécution de code à distance a été découvert dans MariaDB.
Un chemin de recherche non approuvé conduit à une injection <code>eval</code>,
dans laquelle l’utilisateur SUPER de la base de données peut exécuter des
commandes du système d’exploitation après avoir modifié wsrep_provider et
wsrep_notify_cmd.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 10.1.48-0+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mariadb-10.1.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de mariadb-10.1, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/mariadb-10.1">\
https://security-tracker.debian.org/tracker/mariadb-10.1</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2605.data"
# $Id: $
