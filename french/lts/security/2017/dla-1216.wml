#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans wordpress, un outil de blog.
Le projet « Common Vulnerabilities and Exposures » (CVE) identifie les problèmes
suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17091">CVE-2017-17091</a>

<p>wp-admin/user-new.php dans WordPress règle la clef newbloguser à une chaîne
pouvant être directement dérivée de l’identifiant de l’utilisateur. Cela permet
à des attaquants distants de contourner les restrictions d’accès désirées en
entrant cette chaîne.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17092">CVE-2017-17092</a>

<p>wp-includes/functions.php dans WordPress ne demande pas la capacité
unfiltered_html pour le téléversement de fichiers .js. Cela pourrait permettre
à des attaquants distants de mener des attaques XSS à l'aide d'un fichier
contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17093">CVE-2017-17093</a>

<p>wp-includes/general-template.php dans WordPress ne restreint pas correctement
l’attribut de langue d’un élément HTML. Cela pourrait permettre à des attaquants
de mener des attaques XSS à l’aide du réglage de langage d’un site.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17094">CVE-2017-17094</a>

<p>wp-includes/feed.php dans WordPress ne restreint pas correctement les limites
dans des champs RSS ou Atom. Cela pourrait permettre à des attaquants de mener
des attaques XSS à l'aide d'une URL contrefaite.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 3.6.1+dfsg-1~deb7u20.</p>

<p>Nous vous recommandons de mettre à jour vos paquets wordpress.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1216.data"
# $Id: $
