#use wml::debian::translation-check translation="16a228d71674819599fa1d0027d1603056286470" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs problèmes ont été découverts dans wireshark, un analyseur de trafic
réseau. Les dissecteurs de :</p>
<ul>
<li>ISAKMP, un protocole «·Internet Security Association et Key Management
  Protocol·»,</li>
<li>P_MUL, une transmission fiable au protocole multidiffusion,</li>
<li>6LoWPAN, IPv6 sur un réseau sans fil personnel de faible puissance,</li>
</ul>
<p>sont affectés.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5719">CVE-2019-5719</a>

<p>Mateusz Jurczyk a trouvé qu’un bloc de chiffrement manquant dans un paquet
pourrait planter le dissecteur ISAKMP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5717">CVE-2019-5717</a>

<p>Il a été découvert que le disséqueur P_MUL pourrait planter quand un paquet
malformé contient un nombre illégal de 0 dans les données de séquence PDU. Un
tel paquet ne peut être analysé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5716">CVE-2019-5716</a>

<p>Il a été découvert que le dissecteur 6LoWPAN pourrait planter quand un paquet
malformé ne contient pas d’information IPHC bien que l’en-tête indique qu’elle
devrait exister.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 1.12.1+g01b65bf-4+deb8u17.</p>

<p>Nous vous recommandons de mettre à jour vos paquets wireshark.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1645.data"
# $Id: $
