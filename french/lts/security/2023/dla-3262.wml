#use wml::debian::translation-check translation="385fa900a32db33caa8bea518bb988cac2bc44e8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une vulnérabilité potentielle de script
intersite dans smarty3, un moteur de modèles largement utilisé pour PHP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-25047">CVE-2018-25047</a>

<p>Dans Smarty avant la version 3.1.47 et les versions 4.x avant 4.2.1,
libs/plugins/function.mailto.php permettait un script intersite. Une page web
qui utilisait smarty_function_mailto et qui pouvait être paramétrée en
utilisant les paramètres d’entrée GET ou POST, pouvait permettre une injection
de code JavaScript par un utilisateur.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 3.1.33+20180830.1.3a78a21f+selfpack1-1+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets smarty3.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3262.data"
# $Id: $
