#use wml::debian::translation-check translation="4628f1ad938c92ab4c20772e977a845b1f3e1174" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans
golang-go.crypto, les bibliothèques complémentaires de chiffrement de Go.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11840">CVE-2019-11840</a>

<p>Un problème a été découvert dans les bibliothèques complémentaires de
chiffrement de Go, c’est-à-dire golang-googlecode-go-crypto. Si plus de 256 Gio de
séquence de clés (keystream) était généré ou autrement si le compteur dépassait
32 bits, l’implémentation de amd64 générait d'abord une sortie incorrecte, puis revenait
à la séquence clés précédente. Des répétitions d’octets de séquence pouvaient conduire à
une perte de confidentialité dans les applications de chiffrement ou à une
prédictibilité dans les applications CSPRNG.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11841">CVE-2019-11841</a>

<p>Un problème de contrefaçon de message a été découvert dans
crypto/openpgp/clearsign/clearsign.go dans les bibliothèques complémentaires de
chiffrement de Go.
L’en-tête <q>Hash</q> d'Armor spécifie le(s) algorithme(s) de hachage cryptographique
utilisé(s) pour la signature. Étant donné que la bibliothèque omet en général
l’analyse de l’en-tête d’Armor, un attaquant pouvait non seulement intégrer des
en-têtes Armor arbitraires, mais aussi préfixer du texte arbitraire aux messages
en clair sans invalidation des signatures.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9283">CVE-2020-9283</a>

<p>La fonction golang.org/x/crypto permettai un plantage lors de la vérification
de signature dans le paquet golang.org/x/crypto/ssh. Un client pouvait attaquer
un serveur SSH acceptant des clés publiques. De plus, un serveur pouvait
attaquer n’importe quel client SSH.</p>

<p>Les paquets Go suivants ont été reconstruits afin de résoudre les problèmes
mentionnés ci-dessus :</p>

<p>rclone : 1.45-3+deb10u1<br />
obfs4proxy : 0.0.7-4+deb10u1<br />
gobuster : 2.0.1-1+deb10u1<br />
restic : 0.9.4+ds-2+deb10u1<br />
gopass : 1.2.0-2+deb10u1<br />
aptly : 1.3.0+ds1-2.2~deb10u2<br />
dnscrypt-proxy : 2.0.19+ds1-2+deb10u1<br />
g10k : 0.5.7-1+deb10u1<br />
hub : 2.7.0~ds1-1+deb10u1<br />
acmetool : 0.0.62-3+deb10u1<br />
syncthing : 1.0.0~ds1-1+deb10u1<br />
packer : 1.3.4+dfsg-4+deb10u1<br />
etcd : 3.2.26+dfsg-3+deb10u1<br />
notary : 0.6.1~ds1-3+deb10u1</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1:0.0~git20181203.505ab14-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets golang-go.crypto.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de golang-go.crypto,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/golang-go.crypto">\
https://security-tracker.debian.org/tracker/golang-go.crypto</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3455.data"
# $Id: $
