#use wml::debian::translation-check translation="392e7aa860c873eab8af0201a344fab8773f3426" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans Asterisk, un
autocommutateur téléphonique privé (PBX) au code source ouvert. Des dépassements
de tampon et d'autres erreurs de programmation pouvaient être exploités pour
lancer une attaque par déni de service ou l'exécution de code arbitraire.</p>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1:16.28.0~dfsg-0+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets asterisk.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de asterisk,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/asterisk">\
https://security-tracker.debian.org/tracker/asterisk</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3335.data"
# $Id: $
