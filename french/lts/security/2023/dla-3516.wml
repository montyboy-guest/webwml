#use wml::debian::translation-check translation="94f44d79ab11a472b9792b8958b0591444990646" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans la version de yajl fournie
avec burp, un programme simple de sauvegarde et restauration multiplateforme par
le réseau. yajl est un analyseur JSON et un petit générateur JSON de
validation.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16516">CVE-2017-16516</a>

<p>Quand un fichier JSON contrefait était fourni à yajl, le processus pouvait
planter avec un SIGABRT dans la fonction yajl_string_decode dans yajl_encode.c.
Cela pouvait éventuellement aboutir à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24795">CVE-2022-24795</a>

<p>Les branches 1.x et 2.x  de <q>yajl</q> contenait un dépassement d'entier qui
conduisait à une corruption de mémoire de tas lors du traitement de grandes
entrées (~2 Go).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-33460">CVE-2023-33460</a>

<p>Une fuite de mémoire existait dans yajl 2.1.0 avec l’utilisation de la
fonction yajl_tree_parse, qui éventuellement faisait que le serveur épuisait sa
mémoire et plantait.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 2.1.32-2+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets burp.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de burp,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/burp">\
https://security-tracker.debian.org/tracker/burp</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3516.data"
# $Id: $
