#use wml::debian::translation-check translation="47bf38472e10758572743728f86621b86187bfa9" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une attaque potentielle par déni de service
dans Django, le cadriciel populaire de développement basé sur Python.</p>

<p><code>EmailValidator</code> et <code>URLValidator</code> étaient susceptibles
d’une attaque par déni de service par expression rationnelle à l'aide d'un grand
nombre d’étiquettes de nom de domaine de courriels et d’URL.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-36053">CVE-2023-36053</a>

<p>Dans Django versions 3.2 avant 3.2.20, 4 avant 4.1.10 et 4.2 avant 4.2.3,
EmailValidator et URLValidator étaient susceptibles d’une attaque ReDoS (déni de
service par expression rationnelle) à l'aide d'un grand nombre d’étiquettes de
nom de domaine de courriels et d’URL.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1:1.11.29-1+deb10u9.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-django.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3500.data"
# $Id: $
