#use wml::debian::translation-check translation="436b550caa7f6880219058658a66867c5658a92e" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Cette mise à jour inclut les modifications dans tzdata 2023c pour les
liaisons Perl. Pour la liste des modifications, consultez la DLA-3412-1.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1:2.23-1+2023c.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libdatetime-timezone-perl.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libdatetime-timezone-perl,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libdatetime-timezone-perl">\
https://security-tracker.debian.org/tracker/libdatetime-timezone-perl</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3413.data"
# $Id: $
