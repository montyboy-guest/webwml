  <h2>Er alt dette <a href="free" name="free">fritt tilgjengelig?</a></h2>

  <p>
    Når vi bruker ordet "fri", mener vi
    programvare-<strong>"frihet"</strong>, ikke "gratis".  Du kan lese
    mer om <a href="free">hva vi mener med "fri programvare"</a> og
    <a href="https://www.gnu.org/philosophy/free-sw">hva Free
      Software Foundation sier</a> om temaet.</p>

  <p>
    Kanskje undrer du på: Hvorfor vil folk bruke time etter time av
    sin egen tid til å skrive programvare, omstendelig pakke det, og
    deretter <em>gi</em> alt dette bort?  Svarene er like varierte som
    folkene som bidrar.  Noen liker å hjelpe andre.  Mange skriver
    programmer for å lære mer om datamaskiner.  Flere og flere ser etter
    måter å unngå oppblåste prisene på programvare.  En
    voksende flokk bidrar som en takk for all den programvaren de har
    fått gratis og fritt fra andre.  Mange i akademiske sirkler lager
    fri programvare for å hjelpe til med å få utbredt resultatene av
    forskningen sin til allmenn bruk.  Bedrifter hjelper med å
    vedlikeholde fri programvare slik at de kan ha et ord med i hvordan
    det utvikles -- det fins ingen raskere måte å få inn en ny funksjon
    enn å implementere det selv!  Selvsagt er det mange av oss som
    bidrar fordi vi liker å holde på med det.</p>

  <p>
    Debian er til den grad tilegnet fri programvare at vi syntes det
    ville være nyttig om dette var formalisert i et slags dokument.
    Dermed ble vår <a href="$(HOME)/social_contract">sosiale
    kontrakt</a> født.</p>

  <p>
    Selv om Debian tror på fri programvare, fins det forhold hvor folk
    vil ha eller trenger ufri programvare på maskinen sin.  Hvor det
    er mulig støtter Debian dette.  Det fins en økende menge av pakker
    hvis eneste rolle er å installere ufri programvare på et
    Debiansystem.</p>

